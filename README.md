# gamryRemoteControl

This is a TCP/IP remote control interface for the Gamry reference 600 potentiostat controlled by the Gamry frameworks software that makes its possible to use the potentiostat in a greater lab system with its own control system by sending simple socket calls to the computer controlling the Gamry device.

its basically just a socket server controlling and running scripts inside the frameworks environment

