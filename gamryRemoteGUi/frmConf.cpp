//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: frmConf.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "frmConf.h"
#include "resource.h" 
#include <io.h> 
#include <stdlib.h>
#include <fcntl.h>
#include <iostream>
#include <ios>

#include "common.h"
//#include "prsht.h"
//#pragma comment (lib, "comctl32")

// perhaps construct a class instead 
// what the class should do, 
// read in configurations files in eis300 file 
// construct sheets depending of the amount of section found in eis300
// under each sheet  add editcontrol boxes and name line 
// if push apply or OK all input feils are saved in the eis300 file
// remeber that time feild in conf file should also be able to fill out 
// perhaps one should be able to do a time test and record the used time for a specifick funktion takes

PROPSHEETHEADER	m_PropSheet;
PROPSHEETPAGE m_psp[3];
LOGFONT m_lfont;

int APIENTRY configPage(HINSTANCE hInstance)
{
	memset(m_psp, 0, sizeof(m_psp));
    memset(&m_PropSheet, 0, sizeof(m_PropSheet));

	m_psp[0].dwSize = sizeof(PROPSHEETPAGE);
    m_psp[0].dwFlags = PSP_DEFAULT|PSP_USETITLE;
    m_psp[0].hInstance = hInstance;
    m_psp[0].pszTemplate = (LPCSTR)IDD_PROPPAGE_MEDIUM; 
    m_psp[0].pszTitle = "EIS setup";

    m_psp[1].dwSize = sizeof(PROPSHEETPAGE);
    m_psp[1].dwFlags = PSP_USETITLE;
    m_psp[1].hInstance = hInstance;
    m_psp[1].pszTemplate = (LPCSTR)IDD_PROPPAGE_MEDIUM1;
    m_psp[1].pszTitle = "ESP script setup";

	m_psp[2].dwSize = sizeof(PROPSHEETPAGE);
    m_psp[2].dwFlags = PSP_USETITLE;
    m_psp[2].hInstance = hInstance;
    m_psp[2].pszTemplate = (LPCSTR)IDD_PROPPAGE_MEDIUM2;
    m_psp[2].pszTitle = "RAW Manuel Edit";
	 m_psp[0].pfnDlgProc = (DLGPROC)Page3DlgProc;
    //m_psp[0].pfnCallback = (LPFNPSPCALLBACK) Page1Proc;

	m_PropSheet.dwSize = sizeof(PROPSHEETHEADER);
    m_PropSheet.dwFlags = PSH_PROPSHEETPAGE;
    m_PropSheet.hInstance = hInstance;
    m_PropSheet.pszCaption = (LPSTR) "Properties";
    m_PropSheet.nPages = 3;
    m_PropSheet.nStartPage = 2;
    m_PropSheet.ppsp = (LPCPROPSHEETPAGE)m_psp;
	m_PropSheet.pfnCallback = (PFNPROPSHEETCALLBACK) SheetProc;

    PropertySheet(&m_PropSheet);
    return 0;
}


// callback function for property sheet
LRESULT CALLBACK SheetProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	//ShowWindow(GetDlgItem(hDlg, IDOK), SW_HIDE);
	//ShowWindow(GetDlgItem(hDlg, IDCANCEL), SW_HIDE);
    //if you want to hide the Apply but i did not hide it to do more things
	//ShowWindow(GetDlgItem(hDlg, ID_APPLY_NOW), SW_HIDE);

    //PropSheet_GetTabControl(hDlg) \
    // (HWND)SNDMSG(hDlg, PSM_GETTABCONTROL, 0, 0)


	// set a bold font to the tabs
	m_lfont.lfHeight         = 8;
	m_lfont.lfWeight         = FW_NORMAL;
	m_lfont.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	//strcpy(m_lfont.lfFaceName, _T("Arial"));
	HWND hTabCtrl = PropSheet_GetTabControl(hDlg);
	SetDlgItemText(hDlg, IDC_EDIT_SETUP_PROB, "test");
	SendMessage(hTabCtrl, WM_SETFONT, (WPARAM)&m_lfont, 0);
	switch( message )
	{
		case WM_INITDIALOG:
			
			return TRUE;
	}
    return FALSE;
}


// Dialog proc for page 1
LRESULT CALLBACK Page3DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
		case WM_INITDIALOG:
			{
				// read in EIS300 file and show it in edit box
				CkString fileContens;
				fileContens = GamryPtrInterface->loadRawFile_EIS300(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName())); 
				HWND hEdit = GetDlgItem(hDlg, IDC_EDIT_SETUP_PROB);
				SetWindowText(hEdit, (LPCSTR) "test om virker");
				return TRUE;
			}
		case WM_COMMAND:
			//if the textboxes are changed
			if (HIWORD(wParam) == EN_CHANGE) {
				SendMessage(GetParent(hDlg), PSM_CHANGED, (WPARAM)hDlg, 0);
			}
			break;
		case WM_NOTIFY:
			switch (((NMHDR FAR *) lParam)->code) {
			case PSN_APPLY:
				//user clicked APPLY
				
				break;
			case PSN_KILLACTIVE: 
				//user clicked OK or selected another page
				
				
				break;				
			}
	}
    return FALSE;
}