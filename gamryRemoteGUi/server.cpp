//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: server.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "server.h"
#include <CkGlobal.h>

remoteGamryServer::remoteGamryServer(void)
{
     
/*
	CkGlobal glob;
	bool success = glob.UnlockBundle("Anything for 30-day trial");
	if (success != true) {
		std::cout << glob.lastErrorText() << "\r\n";
		return;
	}
	CkSocket listenSocket;
	*/

    success = listenSocket.UnlockComponent("Anything for 30-day trial");
    if (success != true) {
        printf("%s\n",listenSocket.lastErrorText());
        
    }

	impedanceRunFlag = false; 
    
}    
 


bool remoteGamryServer::startServer(gamryControl *controlDevice)
{
	
	PtrcontrolDevice = controlDevice; // save a loacal pointer to the device control + 
	CkString ip;
	listenSocket.get_MyIpAddress(ip);
	unsigned int port = controlDevice->getEthPort();
	controlDevice->setEthsettings((char*) ip.getString(), port);
	
	success = listenSocket.BindAndListen(port,DEFAULT_connections);
    if (success != true) {
        printf("%s\n",listenSocket.lastErrorText());
        return false;
    }
	

	printf(" starting remote server waiting for connection\n");
	bool exit = 0;
	while(! exit){
    //  Get the next incoming connection
    //  Wait infinite 

	msgOut.clear(); 
	
    CkSocket *connectedSocket = 0;
	CkStringArray *argVector;		// argument vector recived msg is split up in this array with instr as the first agument
    connectedSocket = listenSocket.AcceptNextConnection(0);
    if (connectedSocket == 0 ) {
        printf("%s\n",listenSocket.lastErrorText());
        return false;
    }
	
    //  Set maximum timeouts for reading an writing (in millisec)
    connectedSocket->put_MaxReadIdleMs(DEFAULT_timeout);
    connectedSocket->put_MaxSendIdleMs(DEFAULT_timeout);

	
	// get recieved data and print out
	
	const char * response;
    response = connectedSocket->receiveUntilMatch("\n\n");
    if (response == 0 ) {
		std::cout << connectedSocket->lastErrorText() << std::endl;
        return false;
    }
	else{		// split up msg in argument vector(array)

			CkString str;
			
			str.append(response);
			std::cout << "msg: " << str.getString() << std::endl;
			
			bool exceptDoubleQuoted = true;
			bool exceptEscaped = true;	// Do not treat characters preceded with a backslash as a delimiter.
			bool keepEmpty = false;	// Do not keep empty fields.
			char delimiter = ' ';		// space 
			argVector = str.split(delimiter,exceptDoubleQuoted,exceptEscaped,keepEmpty);
			//std::cout <<  "msg arg NUM: " << argVector->get_Count() << std::endl;
		
			if(strcmp( argVector->getString(0),"impedance") == 0 )
			{
				
					CkString sessionNum; 
					argVector->GetString(3,sessionNum); // 
					connectedSocket->SendString( controlDevice->eis(argVector->getString(1),(unsigned int) sessionNum.intValue()));
				
			}
			else if(strcmp( argVector->getString(0),"chrono") == 0 )
			{
				
					CkString sessionNum; 
					argVector->GetString(3,sessionNum); // 
					connectedSocket->SendString( controlDevice->chrono(argVector->getString(1),(unsigned int) sessionNum.intValue()));
				
			}
			else if(strcmp( argVector->getString(0),"potsweep") == 0 )
			{
				
					CkString sessionNum; 
					argVector->GetString(3,sessionNum); // 
					connectedSocket->SendString( controlDevice->cyclicVoltammetry(argVector->getString(1),(unsigned int) sessionNum.intValue()));
				
			}
			else if(strcmp( argVector->getString(0),"get_file") == 0 )
			{
				
					CkString sessionNum; 
					CkString fileNum;
					argVector->GetString(3,sessionNum); // 
					argVector->GetString(4,fileNum); // 
					// allways EIS mode    only work with that for now 
					connectedSocket->SendString( controlDevice->getNewFile(argVector->getString(1),(unsigned int) sessionNum.intValue(),fileNum.intValue(),"EIS"));
				
			}

			else if(strcmp( argVector->getString(0),"get_file_new") == 0 )
			{
				
					CkString sessionNum; 
					CkString fileNum;
					argVector->GetString(3,sessionNum); // 
					argVector->GetString(4,fileNum); // 
					// allways EIS mode    only work with that for now 
					// this function will not return measuring   i now it no logic  but s�rens K difinition of getfile_new or not is inverted vise vase
					connectedSocket->SendString( controlDevice->getFile(argVector->getString(1),(unsigned int) sessionNum.intValue(),fileNum.intValue(),"EIS"));
				
			}

			else if(strcmp(argVector->getString(0) , "ping") == 0 )
			{
				std::cout << " ping cmd recieved" << std::endl;
				connectedSocket->SendString( ping() );
			}
			else if(strcmp(argVector->getString(0) , "mode") == 0 )
			{
				connectedSocket->SendString( "gamry" );
			}
			else if(strcmp(argVector->getString(0) , "exit") == 0)
			{
				std::cout << "remote exit call " << std::endl;
				connectedSocket->SendString("exiting server");
				
				exit = 1;

			}
			else if(strcmp(argVector->getString(0) , "stop_impedance") == 0)
			{
				std::cout << "stop measurement"<< std::endl;
				// stop measurment rutine goes here
				connectedSocket->SendString("cant stop measurment remotely");		// send to client  when stopped 
			}
			else if(strcmp(argVector->getString(0) , "set_DCcurrent") == 0){ 
			// 
				CkString sessionNum; 
				argVector->GetString(4,sessionNum); // 
				
				connectedSocket->SendString( controlDevice->setVoltage(argVector->getString(2),(unsigned int) sessionNum.intValue(), argVector->getString(1)));
			}
			else if(strcmp(argVector->getString(0) , "set_DCpotential") == 0){ 
			//
				
				CkString sessionNum; 
				argVector->GetString(4,sessionNum); // 
				
				connectedSocket->SendString( controlDevice->setVoltage(argVector->getString(2),(unsigned int) sessionNum.intValue(), argVector->getString(1)));
				
			}
			else if(strcmp(argVector->getString(0) , "measure_DCcurrent") == 0){ 
			//
				CkString sessionNum; 
				argVector->GetString(3,sessionNum); // 
				connectedSocket->SendString( controlDevice->measureCurrent(argVector->getString(1),(unsigned int) sessionNum.intValue()));
			}
			else if(strcmp(argVector->getString(0) , "measure_DCpotential") == 0){ 
			// 
				CkString sessionNum; 
				argVector->GetString(3,sessionNum); // 
				connectedSocket->SendString( controlDevice->measureVoltage(argVector->getString(1),(unsigned int) sessionNum.intValue())); 
			}
			else if(strcmp(argVector->getString(0) , "reset_server") == 0){
				impedanceRunFlag = false;
				
				connectedSocket->SendString("done");
			}
			else{
				// ".exp" is contained in the string with a space after pars this name
				// to the explain script ini file + add any aguments needed 
				
				str.append(argVector->getString(0));
				if(str.containsSubstringNoCase(".exp")){
					// pars string to ini file + any aguments
					CkString sessionNum; 
					argVector->GetString(3,sessionNum); // 
					// perhaps check if sessionNUm is a number 
					connectedSocket->SendString( controlDevice->exp_script( argVector->getString(1),(unsigned int) sessionNum.intValue(),argVector->getString(0)));


				}
				else{
				// else invalid response
					connectedSocket->SendString("invalid response");
				}
				
			}

		}
	

	
    connectedSocket->Close(10);

    // printf("success!\n");

    }
	 
	// 
	// 
 return false; 
}



bool remoteGamryServer::EISrunning(void)		// clean up and make sure to catch EIS done command from zahner
{	// not in use
	bool status = false ;
	
	status = true ;
	
	return status;
}


const char * remoteGamryServer::ping(void)
{
	//
	msgOut.append("gamry Impedance analyzer on $hostname on addr:$host listening on port $port");
	return msgOut.getString() ;
}


const char * remoteGamryServer::getIP() // perhaps update it self 
{
	
	CkString ip;
	listenSocket.get_MyIpAddress(ip);
	unsigned int port = PtrcontrolDevice->getEthPort();
	PtrcontrolDevice->setEthsettings((char*) ip.getString(), port);
	return ip; 
}