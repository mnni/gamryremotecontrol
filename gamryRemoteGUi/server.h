//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: server.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef SERVER_H_INCLUDED__
#define SERVER_H_INCLUDED__

#include "gamrycontrol.h"
#include <CkSocket.h>

#include <stdio.h>
#include <string.h>

#include <CkStringArray.h>
#include <CkString.h>



class remoteGamryServer
{
	private:	

		static const int DEFAULT_PORT = 4040;	//  
		static const int DEFAULT_connections = 2;	// 
		static const int DEFAULT_timeout = 20000;
		
		CkString msgOut;
		CkSocket listenSocket;

		gamryControl *PtrcontrolDevice;

				// instance of intercface module goes here
		bool success;
		bool impedanceRunFlag;

		
		// get file
		bool EISrunning();
		const char * ping();

	public:
	const char * getIP();
	remoteGamryServer();
	bool startServer(gamryControl *controlDevice);
};
#endif