//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: Gamrycontrol.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////
#include "gamrycontrol.h"




gamryControl::gamryControl( const char *homeFolderPath)
{
     // "c:\\GamryRemoteCTRL"; 
	homeFolder.append(homeFolderPath);
	createHomeFolders(); // write home folder and user folder if its first time running
	std::stringstream ss;
	ss << homeFolderPath << "\\user.INI" ;
	std::string s = ss.str();
	std::cout<<"roth path: " <<s.c_str()<<std::endl;		// debugger
	
	
	CIniReader INIRead((char *) s.c_str());  
	CIniWriter INIWrite((char *) s.c_str()); 
	
	 
	UserINIRead = INIRead;
	UserINIWrite = INIWrite;
	UserINIWrite.WriteString("SYS", "STATE", "IDLE");	// allways set this tag when a server starts up

	ss.str("");
	ss << homeFolderPath << "\\setup.INI" ;
	s = ss.str();
	CIniReader  IniSetupR((char *) s.c_str());
	CIniWriter	IniSetupW((char *) s.c_str());
	
	iniWriterSetup  = IniSetupW;
	iniReaderSetup	= IniSetupR;
	creatDeafaultSetupFile();  // if not allready created 
	
	creatDefaultSessionSetFile(); // if not allready created

}

bool gamryControl::createNewUser(const char* thisUserName)// and log in 
{

	
	// check if user exisit allready
	CkString msg;
	CkString registrated_users; 
	registrated_users = UserINIRead.ReadString("USER","REG_NAMES", "");
	// ;userName;  // key word
	std::stringstream ss;
	ss.str("");
	ss << ";" << thisUserName << ";";
	std::string s = ss.str();
	if(registrated_users.containsSubstringNoCase((char*) s.c_str()))
	{
		//std::cout<< s.c_str() << std::endl;
		// clear the control.ini  file parameters 
		// set user exist flag 
		msg = " user allready excist";
		
		return false;
	}
	else{ 
		
		 
		// write path folder to the user 
		// add userName to the reg_users 
		registrated_users.append((char*) s.c_str());  // add new user name to the reg user string
		UserINIWrite.WriteString("USER","REG_NAMES",(char*) registrated_users.getString());
		
		 
			//	preparing paths to selected user
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName ;
		s = ss.str();
		dirUserPath.clear();
		dirUserPath.append( s.c_str());
		std::cout<<"dirUserPath: " << dirUserPath.getString() << std::endl;
		// path to session control file
		ss.str("");
		ss << dirUserPath.getString() << "\\session_control.INI";
		s = ss.str();
		control_INI_Path.clear();
		control_INI_Path.append(s.c_str());
		//std::cout<<"session_control.INI file: " << (char *)control_INI_Path.getString() << std::endl;

		createUserFolder();	// will only write folder if not there already 
		// create ini R/W instances 
		CIniWriter iniWriter((char *)control_INI_Path.getString());
		CIniReader iniReader((char *)control_INI_Path.getString());
	
		iniWritercontrol = iniWriter;
		iniReadercontrol = iniReader;
	
		UserINIWrite.WriteString("USER","NAME",(char*) thisUserName);	// write user to user.ini  file this is the logget in name
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName); // this is the user that is invoked 
		userName = UserINIRead.ReadString("USER","NAME","DEFAULT_USER");  // use default if no name is specifired 

		// make directory
		createSessionFolders(1); // for session 1
		createSessionConfFile(1,0); // set the session test number  alway 0 when declared 
		createSessionSetFile(1);
		createNewControlIni();	// writing default control.ini  file

		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) control_INI_Path.getString()); // write the path to the session contrl file in use
		//initUserControlIni("10.0.0.1", "4040", 1);

		return true;
	}

}  


bool gamryControl::setUserAndSession( const char* thisUserName, unsigned int thisSessionNum)
{
	// check if user exist  and check if session exist 
	// if true  set user.ini file to the userName and set the ini control file writer/reader to and point on the right control.ini for that user file and set the session number
	// returns true if above is done else false
	// perhaps each session should have its ownen ini control file so it would be possible to use different settings for each session

	if(isValidSession(thisUserName, thisSessionNum)){

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName ;
		std::string s = ss.str();
		dirUserPath.clear();
		dirUserPath.append( s.c_str());
		std::cout<<"dirUserPath: " << dirUserPath.getString() << std::endl;
		// path to session control file
		ss.str("");
		ss << dirUserPath.getString() << "\\session_control.INI";
		s = ss.str();
		control_INI_Path.clear();
		control_INI_Path.append(s.c_str());
		std::cout<<"session_control.INI file: " << (char *)control_INI_Path.getString() << std::endl;
		
		// path to current session conf file
		ss.str("");
		ss << dirUserPath.getString() << "\\" << thisSessionNum <<"\\sessionconf.ini" ;
		s = ss.str();
		conf_ini_path.clear();
		conf_ini_path.append(s.c_str());
		std::cout<<"sessionconf.ini file: " << (char *)conf_ini_path.getString() << std::endl;

		
		
		// create ini R/W instances 
		CIniWriter iniWriter((char *)control_INI_Path.getString());
		CIniReader iniReader((char *)control_INI_Path.getString());
	
		iniWritercontrol = iniWriter;
		iniReadercontrol = iniReader;
	
		UserINIWrite.WriteString("USER","NAME",(char*) thisUserName);	// write user to user.ini  file this is the logget in name
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName); // this is the user that is invoked 
		userName.clear();
		userName = UserINIRead.ReadString("USER","NAME","DEFAULT_USER");  // use default if no name is specifired 
		
		iniWritercontrol.WriteString("SYS","STATE","IDLE");
		iniWritercontrol.WriteInteger("SYS","CurrentSessionNUM",thisSessionNum);
		iniWritercontrol.WriteString("SYS","CurrentSessionConfFile",(char*) conf_ini_path.getString());
		
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) control_INI_Path.getString()); // write the path to the session contrl file in use
		//initUserControlIni("10.0.0.1", "4040", 1);

		return true; 


	}
	else return false;
}

bool gamryControl::setUser(const char* thisUserName)
{
	// get the user session number 
	//  setUserAndSession
	if(isValidUser(thisUserName))
	{
		
		unsigned int currentSessionNum = getCurrentSessionNum(thisUserName);
		setUserAndSession(thisUserName,currentSessionNum);
		return true;
	} 
	return false;
}


void gamryControl::createNewControlIni()
{
	iniWritercontrol.WriteString("SYS", "STATE", "IDLE");		//, IDLE,  FX_SCRIPT, EIS, PSTATE
	iniWritercontrol.WriteString("SYS", "USERNAME", (char*) userName.getString());
	iniWritercontrol.WriteString("SYS", "CurrentSessionNUM", "1");
	iniWritercontrol.WriteString("SYS", "LastSessionNUM", "1");

	// path to current session conf file
		std::stringstream ss;
		ss << dirUserPath.getString() << "\\1\\sessionconf.ini" ;
		std::string s = ss.str();
		conf_ini_path.clear();
		conf_ini_path.append(s.c_str());
		std::cout<<"sessionconf.ini file: " << (char *)conf_ini_path.getString() << std::endl;
		
	iniWritercontrol.WriteString("SYS", "CurrentSessionConfFile", (char *)conf_ini_path.getString());

	iniWritercontrol.WriteString("EIS","DONE","yes" )  ; // YES or BUSY  for use if multiple functions should be run at the same time etc, 
	iniWritercontrol.WriteString("EIS","FILENAME","YES" )  ; //get last filename and add 1
	iniWritercontrol.WriteString("EIS","SETUP_FILE"," " )  ;
	iniWritercontrol.WriteString("EIS","SETUP_NAME"," " )  ;
	iniWritercontrol.WriteString("EIS","PstatNo","1" )  ;  //allways 1 if only one gamry is connected 
	iniWritercontrol.WriteString("EIS","TIME","" )  ;  // estimated time  nummer in seconds
	iniWritercontrol.WriteString("EIS","MODE","POT" )  ;  // what Mode to run GAL or POT
	

	iniWritercontrol.WriteString("EXP_SCRIPT","DONE","YES" )  ; //YES or BUSY  for use if multiple functions should be run at the same time etc, 
	iniWritercontrol.WriteString("EXP_SCRIPT","FILENAME",".something" )  ;// get last filename and add 1
	iniWritercontrol.WriteString("EXP_SCRIPT","SETUP_FILE"," " )  ;
	iniWritercontrol.WriteString("EXP_SCRIPT","SETUP_NAME"," " )  ;
	iniWritercontrol.WriteString("EXP_SCRIPT","PstatNo","1 " )  ;  //allways 1 if only one gamry is connected 
	iniWritercontrol.WriteString("EXP_SCRIPT","TIME","" )  ;  //estimated time  nummer in seconds if any given for the script

	// set and read current operation 
	iniWritercontrol.WriteString("VOLTAGE","MODE","POT" )  ; //POT or GAL ,
	iniWritercontrol.WriteString("VOLTAGE","SET_POINT","0.0" ) ;// set point value when a SET_VOLTAGE command is send this will be the new set point of the Gamry
	iniWritercontrol.WriteString("VOLTAGE","MEASURED_VALUE","0.0" );//   last measured value is stored in this key 

	iniWritercontrol.WriteString("VOLTAGE","SETUP_FILE"," " )  ;
	iniWritercontrol.WriteString("VOLTAGE","SETUP_NAME"," " )  ;

		// set and rread voltage operation 
	iniWritercontrol.WriteString("CURRENT","MODE","GAL" )  ; //POT or GAL ,
	iniWritercontrol.WriteString("CURRENT","SET_POINT","0.0" ) ;// set point value when a SET_CURRENT command is send this will be the new set point of the Gamry
	iniWritercontrol.WriteString("CURRENT","MEASURED_VALUE","0.0" );//   last measured value is stored in this key 

	iniWritercontrol.WriteString("CURRENT","SETUP_FILE"," " )  ;
	iniWritercontrol.WriteString("CURRENT","SETUP_NAME"," " )  ;

	iniWritercontrol.WriteString("CHRONO","DONE","YES" )  ; //YES or BUSY  for use if multiple functions should be run at the same time etc, 
	iniWritercontrol.WriteString("CHRONO","FILENAME",".something" )  ;// get last filename and add 1
	iniWritercontrol.WriteString("CHRONO","SETUP_FILE"," " )  ;
	iniWritercontrol.WriteString("CHRONO","SETUP_NAME"," " )  ;
	iniWritercontrol.WriteString("CHRONO","PstatNo","1 " )  ;  //allways 1 if only one gamry is connected 
	iniWritercontrol.WriteString("CHRONO","TIME","" )  ;  //estimated time  nummer in seconds if any given for the script
	iniWritercontrol.WriteString("CHRONO","MODE","POT" )  ;  // what Mode to run AMP or POT

	iniWritercontrol.WriteString("CYCLIC_VOLTAMMETRY","DONE","YES" )  ; //YES or BUSY  for use if multiple functions should be run at the same time etc, 
	iniWritercontrol.WriteString("CYCLIC_VOLTAMMETRY","FILENAME",".something" )  ;// get last filename and add 1
	iniWritercontrol.WriteString("CYCLIC_VOLTAMMETRY","SETUP_FILE"," " )  ;
	iniWritercontrol.WriteString("CYCLIC_VOLTAMMETRY","SETUP_NAME"," " )  ;
	iniWritercontrol.WriteString("CYCLIC_VOLTAMMETRY","PstatNo","1 " )  ;  //allways 1 if only one gamry is connected 
	iniWritercontrol.WriteString("CYCLIC_VOLTAMMETRY","TIME","" )  ;  //estimated time  nummer in seconds if any given for the script
	
	
} 

void gamryControl::resetUserControlIni() // reset all initfiles to idle mode 
{
	// !!!!!!! !!!!! 
	// get userlist  from user.ini 
	// run through all users and there session_control.ini file 
	// reset them to "idle"
	CkString thisUser;
	CkStringArray userList;  
	userList.SplitAndAppend(UserINIRead.ReadString("USER","REG_NAMES",""),";");
	UserINIWrite.WriteString("SYS", "STATE", "IDLE");	// allways set this tag when a server starts up 
		
	bool end =1 ;
	while(end)
	{
		end = userList.Pop(thisUser);
		std::cout<< thisUser.getString() <<std::endl;		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUser << "\\session_control.INI";
		std::string s = ss.str();
		CIniWriter wSessionControl((char *) s.c_str());
		wSessionControl.WriteString("SYS", "STATE", "IDLE");
	}
	
	
}
	



unsigned int gamryControl::incrSessionNum()
{
	// incriment session number and write it to session_control ini file to both current session tag and last last session number
	int sessionNum = iniReadercontrol.ReadInteger( "SYS", "LastSessionNUM",1);
	iniWritercontrol.WriteInteger("SYS", "CurrentSessionNUM",sessionNum +1);
	iniWritercontrol.WriteInteger("SYS", "LastSessionNUM",sessionNum +1);
	// add new session folder
	createSessionFolders(sessionNum +1);
	createSessionConfFile(sessionNum +1,0);
	createSessionSetFile(sessionNum +1);
	// write new sessionconf.ini file with default settings 
	return sessionNum +1;	// return new session number 
}

bool gamryControl::setSessionNum(unsigned int sessionNum) // this function only use with current user!! should maybe allso take userName
{
	
		std::stringstream ss;
		ss << dirUserPath.getString() << "\\" << sessionNum <<"\\sessionconf.ini" ;
		std::string s = ss.str();
		conf_ini_path.clear();
		conf_ini_path.append(s.c_str());
		//std::cout<<"sessionconf.ini file: " << (char *)conf_ini_path.getString() << std::endl;

	// set session number to control.ini file 
	if(sessionNum <= (unsigned int) iniReadercontrol.ReadInteger( "SYS", "LastSessionNUM",0)){

		iniWritercontrol.WriteInteger("SYS", "CurrentSessionNUM",sessionNum);
		iniWritercontrol.WriteString("SYS", "CurrentSessionConfFile", (char *)conf_ini_path.getString());
		// set allso sessionConf file path 
		return true;
	}
	else{
		// iniWritercontrol.WriteInteger("SYS", "CurrentSessionNUM",iniReadercontrol.ReadInteger( "SYS", "LastSessionNUM",1));
		return false;
	}
}

unsigned int gamryControl::getTotalNumSession(const char* thisUserName)
{
	//returns the amount of sessions for the user profile 
	std::stringstream ss;
	ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI";
	std::string s = ss.str();
	CIniReader rSessionControl((char *) s.c_str());

	return (unsigned int) rSessionControl.ReadInteger( "SYS", "LastSessionNUM",0);
}

unsigned int gamryControl::getCurrentSessionNum(const char* thisUserName)
{
	// returns the current session number 
	std::stringstream ss;
	ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI";
	std::string s = ss.str();
	CIniReader rSessionControl((char *) s.c_str());

	return rSessionControl.ReadInteger( "SYS", "CurrentSessionNUM",0);
	

}

CkString gamryControl::getCurrentUserName(void)
{
	return userName;

}



void gamryControl::createHomeFolders()
{
	CkString pathmkdir;
	pathmkdir = homeFolder.getString();
	mkdir(pathmkdir.getString());
	pathmkdir.append("\\users");
	mkdir(pathmkdir.getString());

}

void gamryControl::createUserFolder()
{
	CkString pathmkdir;
	pathmkdir = dirUserPath.getString();
	std::cout << pathmkdir.getString() << std::endl;
	mkdir(pathmkdir.getString());
}

void gamryControl::createSessionFolders(unsigned int sessionNum)
{
	CkString pathmkdir;
	pathmkdir = dirUserPath.getString();
	pathmkdir.append("\\");
	pathmkdir.appendInt(sessionNum);
	mkdir(pathmkdir.getString());

}

void gamryControl::createSessionConfFile(unsigned int sessionNum, unsigned int testnumber)
{
CkString pathConfFile;
pathConfFile = dirUserPath.getString();
pathConfFile.append("\\");
pathConfFile.appendInt(sessionNum);
CkString pathSetfile;
pathSetfile = pathConfFile;
pathSetfile.append("\\EIS300.set");
pathConfFile.append("\\sessionconf.ini");

CIniWriter WSessionConf((char*) pathConfFile.getString());
CIniReader RSessionConf((char*) pathConfFile.getString());
iniWriterSessionConf = WSessionConf;
iniReaderSessionConf = RSessionConf;

iniWritercontrol.WriteString("SYS","CurrentSessionConfFile",(char*) pathConfFile.getString()); 

CIniWriter sessionConfini((char*) pathConfFile.getString());
sessionConfini.WriteInteger("SESSION", "SESSION_NUMBER" , sessionNum);
sessionConfini.WriteInteger("SESSION", "TEST_NUMBER" , testnumber);

sessionConfini.WriteString("EIS","SETUP_FILE", (char*) pathSetfile.getString());
sessionConfini.WriteString("EIS","SETUP_NAME",iniReaderSetup.ReadString("EIS", "SETUP_NAME","EIS"));
sessionConfini.WriteString("EIS","TIME",iniReaderSetup.ReadString("EIS","TIME","300"));
sessionConfini.WriteString("EIS","MODE",iniReaderSetup.ReadString("EIS","MODE","POT"));

sessionConfini.WriteString("EXP_SCRIPT","SETUP_FILE",(char*) pathSetfile.getString());
sessionConfini.WriteString("EXP_SCRIPT","SETUP_NAME",iniReaderSetup.ReadString("EXP_SCRIPT","SETUP_NAME","EXP_SCRIPT"));
sessionConfini.WriteString("EXP_SCRIPT","TIME",iniReaderSetup.ReadString("EXP_SCRIPT","TIME","300"));

sessionConfini.WriteString("CHRONO","SETUP_FILE",(char*) pathSetfile.getString());
sessionConfini.WriteString("CHRONO","SETUP_NAME",iniReaderSetup.ReadString("CHRONO","SETUP_NAME","CHRONO"));
sessionConfini.WriteString("CHRONO","TIME",iniReaderSetup.ReadString("CHRONO","TIME","300"));
sessionConfini.WriteString("CHRONO","MODE",iniReaderSetup.ReadString("CHRONO","MODE","POT"));

sessionConfini.WriteString("CYCLIC_VOLTAMMETRY","SETUP_FILE",(char*) pathSetfile.getString());
sessionConfini.WriteString("CYCLIC_VOLTAMMETRY","SETUP_NAME",iniReaderSetup.ReadString("CYCLIC_VOLTAMMETRY","SETUP_NAME","CYCLIC_VOLTAMMETRY"));
sessionConfini.WriteString("CYCLIC_VOLTAMMETRY","TIME",iniReaderSetup.ReadString("CYCLIC_VOLTAMMETRY","TIME","300"));


}
void gamryControl::createSessionSetFile(unsigned int sessionNum)
{
	// take a copy of the default set file located in homefolder and place it in session folder

	CkString defaultFileName; 
	defaultFileName	= homeFolder;
	defaultFileName.append("\\Default_EIS300.set");	
	defaultFileName.loadFile(defaultFileName.getString(),"us-ascii");

	CkString pathmkdir;
	pathmkdir = dirUserPath.getString();
	pathmkdir.append("\\");
	pathmkdir.appendInt(sessionNum);
	pathmkdir.append("\\EIS300.set");
	
	defaultFileName.saveToFile(pathmkdir.getString(),"us-ascii");
}

void gamryControl::creatDeafaultSetupFile()
{
	// this function creats a default session control.ini file   with default settings  that can be manual changed after 
	// server has been rand fist time 
	// will only creat if the file as not been created before consider to destribute this file with the exe server file 

	CkString defaultFileName; 
	defaultFileName	= homeFolder;
	defaultFileName.append("\\setup.INI");
	

	CkString defaultFileStream; 
	if(! defaultFileStream.loadFile(defaultFileName.getString(),"us-ascii"))
	{
		// write default setting file 
		
		
		iniWriterSetup.WriteString("SYS","PORT","4040");
		iniWriterSetup.WriteString("SYS","HOSTNAME","DEFAULT_GAMRY");

		iniWriterSetup.WriteString("EIS","SETUP_FILE","Eis300.set");
		iniWriterSetup.WriteString("EIS","SETUP_NAME","EIS");
		iniWriterSetup.WriteString("EIS","TIME","300");
		iniWriterSetup.WriteString("EIS","MODE","POT");
		iniWriterSetup.WriteString("EIS","FILE_EXTENSION",".DTA");
		
		iniWriterSetup.WriteString("EXP_SCRIPT","SETUP_FILE","Eis300.set");
		iniWriterSetup.WriteString("EXP_SCRIPT","SETUP_NAME","EXP_SCRIPT");
		iniWriterSetup.WriteString("EXP_SCRIPT","TIME","300");
		iniWriterSetup.WriteString("EXP_SCRIPT","FILE_EXTENSION",".DTA");

		iniWriterSetup.WriteString("CHRONO","SETUP_FILE","Eis300.set");
		iniWriterSetup.WriteString("CHRONO","SETUP_NAME","CHRONO");
		iniWriterSetup.WriteString("CHRONO","TIME","300");
		iniWriterSetup.WriteString("CHRONO","MODE","POT");
		iniWriterSetup.WriteString("CHRONO","FILE_EXTENSION",".DTA");


		iniWriterSetup.WriteString("CYCLIC_VOLTAMMETRY","SETUP_FILE","Eis300.set");
		iniWriterSetup.WriteString("CYCLIC_VOLTAMMETRY","SETUP_NAME","CYCLIC_VOLTAMMETRY");
		iniWriterSetup.WriteString("CYCLIC_VOLTAMMETRY","TIME","300");
		iniWriterSetup.WriteString("CYCLIC_VOLTAMMETRY","FILE_EXTENSION",".DTA");
		
		// other tags like:  "SYS" "userPath"    , "SYS" "controlFileName  ,  "SYS" "messFILEdir"   etc.   

	}

	

}

void gamryControl::creatDefaultSessionSetFile()
{
	CkString defaultFileName; 
	defaultFileName	= homeFolder;
	defaultFileName.append("\\Default_EIS300.set");	// default file all new session set files will take a copy of this
	

	CkString defaultFileStream; 
	if(! defaultFileStream.loadFile(defaultFileName.getString(),"us-ascii"))
	{
		// write default set file 
		CIniWriter defaultSetFileWriter((char *) defaultFileName.getString());
		
		defaultSetFileWriter.WriteString("INDEX","EIS","");

		defaultSetFileWriter.WriteString("EIS","PSTAT","0");
		defaultSetFileWriter.WriteString("EIS","TITLE","EIS");
		defaultSetFileWriter.WriteString("EIS","NOTES","1");
		defaultSetFileWriter.WriteString("EIS","FREQINIT","100000");
		defaultSetFileWriter.WriteString("EIS","FREQFINAL","1");
		defaultSetFileWriter.WriteString("EIS","PTSPERDEC","10");
		defaultSetFileWriter.WriteString("EIS","IACREQ","0.01");
		defaultSetFileWriter.WriteString("EIS","IDCREQ","0");
		defaultSetFileWriter.WriteString("EIS","AREA","1");
		defaultSetFileWriter.WriteString("EIS","CONDIT","F, 15, 0");
		defaultSetFileWriter.WriteString("EIS","DELAY","T, 3, 10");
		defaultSetFileWriter.WriteString("EIS","ZGUESS","100");
		defaultSetFileWriter.WriteString("EIS","SPEED","1");
		defaultSetFileWriter.WriteString("EIS","VAC","50");
		defaultSetFileWriter.WriteString("EIS","VDC","0, F");


		defaultSetFileWriter.WriteString("INDEX","EXP_SCRIPT","");

		defaultSetFileWriter.WriteString("EXP_SCRIPT","PSTAT","0");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","TITLE","EIS");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","NOTES","1");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","FREQINIT","100000");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","FREQFINAL","1");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","PTSPERDEC","10");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","IACREQ","0.01");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","IDCREQ","0");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","AREA","1");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","CONDIT","F, 15, 0");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","DELAY","T, 3, 10");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","ZGUESS","100");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","SPEED","1");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","VAC","50");
		defaultSetFileWriter.WriteString("EXP_SCRIPT","VDC","0, F");


	}


}

void gamryControl::setEthsettings(char *IP,  unsigned int PORT)
{
	// write the current ethernet settings in the user.ini 
	UserINIWrite.WriteString("SYS","IP",IP);
	UserINIWrite.WriteInteger("SYS","PORT",PORT);

}

unsigned int gamryControl::getEthPort()
{
	return (unsigned int) iniReaderSetup.ReadInteger("SYS","PORT", 4040);

}

CkString gamryControl::getEthIP()
{
	CkString ip;
	ip = UserINIRead.ReadString("SYS","IP", "127.0.0.1");
	return ip;
}



CkString gamryControl::eis(const char* thisUserName , unsigned int sessionNum)
{
	
	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			unsigned int fileNumber = (unsigned int) iniReaderSessionConf.ReadInteger("SESSION","TEST_NUMBER" ,0);
			fileNumber++; 
			iniWriterSessionConf.WriteInteger("SESSION","TEST_NUMBER",fileNumber);
			// write filename with path 
			ss.str("");
			ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\" << fileNumber << iniReaderSetup.ReadString("EIS","FILE_EXTENSION",".DTA");
			s  = ss.str();
			iniWriterSessionCTRL.WriteString("EIS", "FILENAME", (char*) s.c_str());
			
			iniWriterSessionCTRL.WriteString("EIS","SETUP_FILE",iniReaderSessionConf.ReadString("EIS", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("EIS","SETUP_NAME",iniReaderSessionConf.ReadString("EIS", "SETUP_NAME","EIS"));
			iniWriterSessionCTRL.WriteString("EIS","TIME",iniReaderSessionConf.ReadString("EIS","TIME","300"));
			iniWriterSessionCTRL.WriteString("EIS","MODE",iniReaderSessionConf.ReadString("EIS","MODE","POT"));
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "EIS"); // will be cleared by explain script
			
			msg ="Impedance, Mode ";
			msg.append("gamry, Session ");
			msg.appendInt(sessionNum);
			msg.append(", File ");
			msg.appendInt(fileNumber);
			//msg.append(thisUserName);
			msg.append(", Totaltime ");
			msg.appendInt(iniReaderSessionConf.ReadInteger("EIS","TIME", 300)); // default time 300 sec if nothing specifired 
			writeLogFile( thisUserName , sessionNum, msg);	
			return msg;
			//"Impedance, Mode ".get_mode().", Session $session, File $file, Totaltime $totaltime";

		}
		else 	return msg = "server session doing something else";
		// write sesssionConf.ini file number under each session folder see init function 
		// set the "STATE" tag to EIS and set the fileName tag asweell 
		// write sessionEISNUM  tag to the sessionConf.ini file
		// starts from 1  increment everytime a measurment have been done and file writen

		// how to get time user input or file read from a file with same name as the explain script but with another extension
		//iniWritercontrol;
		// return:  $user $session_nr $file_number $time
	}
	return msg = "invalid username  or session number";
}



CkString gamryControl::exp_script(const char* thisUserName , unsigned int sessionNum, const char* scriptName)
{
	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			unsigned int fileNumber = (unsigned int) iniReaderSessionConf.ReadInteger("SESSION","TEST_NUMBER" ,0);
			fileNumber++; 
			iniWriterSessionConf.WriteInteger("SESSION","TEST_NUMBER",fileNumber);
			// write filename with path 
			ss.str("");
			ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\" << fileNumber << iniReaderSetup.ReadString("EXP_SCRIPT","FILE_EXTENSION",".DTA");
			s  = ss.str();
			iniWriterSessionCTRL.WriteString("EXP_SCRIPT", "FILENAME", (char*) s.c_str());
			
			iniWriterSessionCTRL.WriteString("EXP_SCRIPT","RUN_SCRIPTNAME", (char*) scriptName);
			iniWriterSessionCTRL.WriteString("EXP_SCRIPT","SETUP_FILE",iniReaderSessionConf.ReadString("EXP_SCRIPT", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("EXP_SCRIPT","SETUP_NAME",iniReaderSessionConf.ReadString("EXP_SCRIPT", "SETUP_NAME","EXP_SCRIPT"));
			iniWriterSessionCTRL.WriteString("EXP_SCRIPT","TIME",iniReaderSessionConf.ReadString("EXP_SCRIPT","TIME","300"));

			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "EXP_SCRIPT"); // will be cleared by explain script
			
			
			msg.append(thisUserName);
			msg.append(" ");
			msg.appendInt(sessionNum);
			msg.append(" ");
			msg.appendInt(fileNumber);
			msg.append(" ");
			msg.appendInt(iniReaderSessionConf.ReadInteger("EXP_SCRIPT","TIME", 300)); // default time 300 sec if nothing specifired 
			writeLogFile( thisUserName , sessionNum, msg);	
			return msg;

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username or session number";

}




CkString gamryControl::getFile(const char* thisUserName , unsigned int sessionNum , unsigned int fileNumber, const char* modeType) 
{
	// always
	// return the specified file number for the user, session, and type: EIS, other type of mess
	// check if user and session number is valid  else return false or nothing 
	// go to session folder and check if file excist else return  no file with that name 

	CkString filestream;

	if(isValidSession(thisUserName, sessionNum))
	{

		// IF eis MODE STANDARD 
		CkString mode ;
		mode = modeType;

		if(mode.equalsIgnoreCase("EIS")){
			// read file i2b file
			CkString pathFile;
			pathFile = getSessionPath(thisUserName,sessionNum);
			pathFile.appendInt(fileNumber);
			pathFile.append(iniReaderSetup.ReadString("EIS","FILE_EXTENSION",".DTA"));
			
			if(filestream.loadFile(pathFile,"us-ascii")){
				
				filestream = convertDtaToI2b(filestream, pathFile.getString());
			}
			else {
				filestream.append("filename: ");
				filestream.append(pathFile);
				filestream.append(" not found");

			}
		}




	}
	else filestream = "user or session number is wrong\n"; 

	return filestream;
}

CkString gamryControl::getNewFile(const char* thisUserName , unsigned int sessionNum , unsigned int fileNumber, const char* modeType) 
{
		// check if measuring  before getting file 

	CkString filestream;

			
		CkString sysState;
		sysState = UserINIRead.ReadString("SYS","STATE","tag not found");
		
		if(sysState.equalsIgnoreCase("BUSY")){
			filestream.append("MEASURING");		
		}
		else{
			filestream = getFile( thisUserName , sessionNum , fileNumber,  modeType);
		}

		
	return filestream;

}

CkString gamryControl::convertDtaToI2b(CkString dtaFileStream, const char* thisfileName)
{
	// convert DTA impedance filestream to I2b filestream 

	// split line into array of strings
	// loog for keyword: ZCURVE 
	// next line: colom name
	// next line: units 
	// split line into array 
	// count if 11 elements 
	// then  take  freq, real, imag   index value and append to output filestream  with a newline 

	 dtaFileStream.replaceChar(',','.');  // replace all Dk dots with american dot 

	CkStringArray dtalines;
	CkStringArray freq;
	CkStringArray real;
	CkStringArray imag;

	// std::cout << dtaFileStream.getString() << std::endl;   // debug
	dtalines.SplitAndAppend(dtaFileStream, "\r\n");  // CRLF  split   new line
	int index =0;
	//std::cout << " lines read: " << dtalines.get_Length() << std::endl;
	CkString line;
	CkString dateLine;
	CkString timeLine;
	for(int i = 0; i < dtalines.get_Length(); i++){
		CkString str;
		str = dtalines.getString(i);
		if(str.containsSubstring("ZCURVE"))
		{
			index = i;
			dtalines.GetString(index+1,line);	// grap next line with data colonm names
		
		}
		if(str.containsSubstring("DATE"))
		{
			dtalines.GetString(i,dateLine);

		}
		if(str.containsSubstring("TIME"))
		{
			dtalines.GetString(i,timeLine);
		}
		//std::cout << str.getString() << std::endl;
	}
	//std::cout << "ZCURVE found at index: " << index << std::endl;	
	
	
	CkStringArray dateLineArray;
	CkStringArray timeLineArray;
	
	dateLineArray.SplitAndAppend(dateLine,"\t");	// date and time is found in element 2 = A[2] 
	timeLineArray.SplitAndAppend(timeLine,"\t");

	CkStringArray lineSplit;
	lineSplit.SplitAndAppend(line,"\t");
	//std::cout << "line elements: " << lineSplit.get_Length() << std::endl; 
	int dataColomn = lineSplit.get_Length();  // CURVE data elements 
	int dataelements = dataColomn;					// run til not =  data elemts 
	index = index +3;  // start grap data lines ffrom here 
	while(dataelements == dataColomn)		// or to no more elements 
	{
		lineSplit.Clear();
		lineSplit.SplitAndAppend(dtalines.string(index),"\t");
		if(lineSplit.get_Length() == dataColomn){
			freq.Append(lineSplit.getString(2));
			real.Append(lineSplit.getString(3));
			imag.Append(lineSplit.getString(4));
		}
		dataelements = lineSplit.get_Length();
		index++;
	}
	//std::cout << " freq array lenght: " << freq.get_Count() << std::endl;
	CkString i2bFileStream;
	i2bFileStream.append(iniReaderSetup.ReadString("SYS","HOSTNAME","DEFAULT_GAMRY"));
	i2bFileStream.append(":");
	i2bFileStream.append(thisfileName);
	i2bFileStream.append("\n");
	i2bFileStream.append("Desc:\n");
	i2bFileStream.append(dateLineArray.getString(2));
	i2bFileStream.append(":");
	i2bFileStream.append(timeLineArray.getString(2));
	i2bFileStream.append("\n");
	i2bFileStream.append("this file/filestream is a convert of a DTA file for desc of the setup please see ref DTA file\n");
	i2bFileStream.append("\n\n");
	i2bFileStream.appendInt(freq.get_Length());
	i2bFileStream.append("\n");
	int Noutputline = freq.get_Count();
	for(int i = 0; i  < Noutputline ; i++){
		std::stringstream ss;
		ss << freq.getString(i) << " " << real.getString(i) << " " << imag.getString(i) << std::endl;
		std::string s = ss.str();
		i2bFileStream.append( s.c_str()); 
		
	}
	return i2bFileStream;
}




bool gamryControl::isValidUser(const char* thisUserName)
{
	CkString registrated_users; 
	registrated_users = UserINIRead.ReadString("USER","REG_NAMES", "");
	// ;userName;  // key word

	std::stringstream ss;
	ss << ";" << thisUserName << ";";
	std::string s = ss.str();
	if(registrated_users.containsSubstringNoCase((char*) s.c_str())) return true;
	else return false;
}



bool gamryControl::isValidSession(const char* thisUserName, unsigned int thisSessionNum)
{
	// get username file

	CkString registrated_users; 
	registrated_users = UserINIRead.ReadString("USER","REG_NAMES", "");
	// ;userName;  // key word
	// debug lines 
	//std::cout << thisUserName << "  sessionNum: " << thisSessionNum << " reg users:  " << registrated_users.getString() << std::endl;
	//std::cout << homeFolder.getString() << std::endl; //"  " << dirUserPath.getString() << "   " << control_INI_Path.getString() << std::endl;
	std::stringstream ss;
	ss << ";" << thisUserName << ";";
	std::string s = ss.str();
	if(registrated_users.containsSubstringNoCase((char*) s.c_str()))
	{
		// check if session also excist
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		s = ss.str();
		CIniReader iniReader((char *)s.c_str());
		unsigned int maxSessionNum = (unsigned int) iniReader.ReadInteger("SYS","LastSessionNUM", 0);
		if(maxSessionNum >= thisSessionNum)	return true;
		else return false; 

	}
	else{ 
			return false;
	}
	
}

CkString gamryControl::getSessionPath(const char* thisUserName, unsigned int thisSessionNum)
{
	// perhaps check first if valid userf and sessionNUm 

	CkString sessionPath;

	sessionPath.append(homeFolder.getString());
	sessionPath.append("\\users\\");
	sessionPath.append(thisUserName);
	sessionPath.append("\\");
	sessionPath.appendInt(thisSessionNum);
	sessionPath.append("\\");
	return sessionPath;

}
  

bool gamryControl::configSessionConfFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key, char* Value)
{
	// perhaps this function should only invoke  the "" logget in"" user / active user
	// set sessionconf.ini file  to use the correct  gamry setthings    ex.  EIS Potentiostat eller Galvano mode
	// set expected measuring time  setting file to use 
	if(isValidSession( thisUserName, thisSessionNum))
	{

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << thisSessionNum << "\\sessionconf.ini ";
		std::string s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		
		iniWriterSessionConf.WriteString(sectionTag,key,Value);  

		return true; // if  key is set  perhaps   or maybe do not care  expect it to had been set 
	}
	return false; 
}

CkString gamryControl::readSessionConfFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key)
{
	
	CkString keyValue; 
	if(isValidSession( thisUserName, thisSessionNum))
	{

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << thisSessionNum << "\\sessionconf.ini ";
		std::string s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		
		keyValue.append(iniReaderSessionConf.ReadString(sectionTag,key,"key value not found"));  
		return keyValue;
		 // if  key is set  perhaps   or maybe do not care  expect it to had been set 
	}
	
	keyValue = "User name or session number not found"; 
	return keyValue;  
}




bool gamryControl::configSetFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key, char* Value)
{	
	if(isValidSession( thisUserName, thisSessionNum))
	{

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << thisSessionNum << "\\EIS300.set";
		std::string s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		
		iniWriterSessionConf.WriteString(sectionTag,key,Value);  

		return true; // if  key is set  perhaps   or maybe do not care  expect it to had been set 
	}
	return false; 

}

CkString gamryControl::readSetFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key)
{
	CkString keyValue; 
	if(isValidSession( thisUserName, thisSessionNum))
	{

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << thisSessionNum << "\\EIS300.set";
		std::string s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		
		keyValue.append(iniReaderSessionConf.ReadString(sectionTag,key,"key value not found"));  
		return keyValue;
		 // if  key is set  perhaps   or maybe do not care  expect it to had been set 
	}
	
	keyValue = "User name or session number not found"; 
	return keyValue; 

}


void gamryControl::getAllSectionNames(CkStringArray* sectionNameList , const char* thisFile)
{
	CIniReader readIniFile((char *)thisFile);
	readIniFile.getSectionNames(sectionNameList);	
}
		

void gamryControl::getAllSectionKeys(CkStringArray* keyList ,const char* thisFile , const char* sectionName)
{
	CIniReader readIniFile((char *)thisFile);
	CkString tag;
	tag = sectionName;
	return readIniFile.getSectionData(keyList,tag);

}

bool gamryControl::dosectionExists(const char* thisFile, CkString strSection)
{
	CIniReader readIniFile((char *)thisFile);
	if(readIniFile.sectionExists(strSection)) return true;
	else return false;
}


CkString gamryControl::loadFileRaw(const char* thisUserName,unsigned int thisSessionNum, char* FileType)
{
	//std::cout << thisUserName << thisSessionNum << std::endl;
	CkString fileStream;
	if(isValidSession( thisUserName, thisSessionNum))
	{

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << thisSessionNum << "\\" << FileType ;
		std::string s  = ss.str();
		fileStream.loadFile(s.c_str(),"us-ascii");
		 return fileStream;
	}
	
	fileStream = "User name or session number not found"; 
	return fileStream; 

}

CkString gamryControl::loadRawFile_EIS300(const char* thisUserName, unsigned int thisSessionNum)
{
	return loadFileRaw(thisUserName, thisSessionNum, "EIS300.set");

}

CkString gamryControl::loadRawFile_sessionConf(const char* thisUserName, unsigned int thisSessionNum)
{
	return loadFileRaw(thisUserName, thisSessionNum, "sessionconf.ini");

}

bool gamryControl::saveFileRaw(const char* thisUserName,unsigned int thisSessionNum, char* FileType, CkString fileStream)
{
	
	
	if(isValidSession( thisUserName, thisSessionNum))
	{

		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << thisSessionNum << "\\" << FileType ;
		std::string s  = ss.str();
		fileStream.saveToFile(s.c_str(),"us-ascii");
		return true;
		 
	}
	// fail to find user and session 
	return false;

}

bool gamryControl::saveFileRaw_EIS300(const char* thisUserName,unsigned int thisSessionNum, CkString fileStream)
{
	return saveFileRaw(thisUserName, thisSessionNum, "EIS300.set",  fileStream);

}

bool gamryControl::saveFileRaw_sessionConf(const char* thisUserName,unsigned int thisSessionNum, CkString fileStream)
{
	return saveFileRaw(thisUserName, thisSessionNum, "sessionconf.ini",  fileStream);

}

CkString gamryControl::openFrameworkConfMenu(const char* thisUserName , unsigned int sessionNum, const char* menuItem)
{
	// check if another menu is open or something is runnin 
	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			 // 
			if(! strcmp(menuItem, "CONF_EIS_POT")){
				// run pot mode conf
				iniWriterSessionCTRL.WriteString("EIS","SETUP_FILE",iniReaderSessionConf.ReadString("EIS", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("EIS","SETUP_NAME",iniReaderSessionConf.ReadString("EIS", "SETUP_NAME","EISPOT"));
			iniWriterSessionCTRL.WriteString("EIS","TIME",iniReaderSessionConf.ReadString("EIS","TIME","300"));
			iniWriterSessionCTRL.WriteString("EIS","MODE",iniReaderSessionConf.ReadString("EIS","MODE","POT"));   // should allredy be set

			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE",(char*) menuItem); // will be cleared by explain script
			msg ="please go to framwork to config the menu item";
			}
			else if(! strcmp(menuItem, "CONF_EIS_GAL")){
			iniWriterSessionCTRL.WriteString("EIS","SETUP_FILE",iniReaderSessionConf.ReadString("EIS", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("EIS","SETUP_NAME",iniReaderSessionConf.ReadString("EIS", "SETUP_NAME","EISGAL"));
			iniWriterSessionCTRL.WriteString("EIS","TIME",iniReaderSessionConf.ReadString("EIS","TIME","300"));
			iniWriterSessionCTRL.WriteString("EIS","MODE",iniReaderSessionConf.ReadString("EIS","MODE","GAL"));

			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE",(char*) menuItem); // will be cleared by explain script
			msg ="please go to framwork to config the menu item";
			}
			else if(! strcmp(menuItem, "CONF_CHRONOAMP")){
			iniWriterSessionCTRL.WriteString("CHRONO","SETUP_FILE",iniReaderSessionConf.ReadString("CHRONO", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CHRONO","SETUP_NAME",iniReaderSessionConf.ReadString("CHRONO", "SETUP_NAME","CHRONO"));
			iniWriterSessionCTRL.WriteString("CHRONO","TIME",iniReaderSessionConf.ReadString("CHRONO","TIME","300"));
			iniWriterSessionCTRL.WriteString("CHRONO","MODE",iniReaderSessionConf.ReadString("CHRONO","MODE","AMP"));

			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE",(char*) menuItem); // will be cleared by explain script
			msg ="please go to framwork to config the menu item";
			}
			else if(! strcmp(menuItem, "CONF_CYCLIC_VOLTAMMETRY")){
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY","SETUP_FILE",iniReaderSessionConf.ReadString("CYCLIC_VOLTAMMETRY", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY","SETUP_NAME",iniReaderSessionConf.ReadString("CYCLIC_VOLTAMMETRY", "SETUP_NAME","CYCLIC_VOLTAMMETRY"));
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY","TIME",iniReaderSessionConf.ReadString("CYCLIC_VOLTAMMETRY","TIME","300"));
			

			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE",(char*) menuItem); // will be cleared by explain script
			msg ="please go to framwork to config the menu item";
			}
			else if(! strcmp(menuItem, "CONF_CHRONOPOT")){
			iniWriterSessionCTRL.WriteString("CHRONO","SETUP_FILE",iniReaderSessionConf.ReadString("CHRONO", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CHRONO","SETUP_NAME",iniReaderSessionConf.ReadString("CHRONO", "SETUP_NAME","CHRONO"));
			iniWriterSessionCTRL.WriteString("CHRONO","TIME",iniReaderSessionConf.ReadString("CHRONO","TIME","300"));
			iniWriterSessionCTRL.WriteString("CHRONO","MODE",iniReaderSessionConf.ReadString("CHRONO","MODE","POT"));

			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE",(char*) menuItem); // will be cleared by explain script
			msg ="please go to framwork to config the menu item";
			}	

			else msg = " Error MenuItem not known";
			
			
			return msg;
			//"Impedance, Mode ".get_mode().", Session $session, File $file, Totaltime $totaltime";

		}
		else 	return msg = "server session doing something else cant start framework menu item please close framwork menu item if";
		
	}
	return msg = "invalid username  or session number";
}



CkString gamryControl::chrono(const char* thisUserName , unsigned int sessionNum)
{
	
	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			unsigned int fileNumber = (unsigned int) iniReaderSessionConf.ReadInteger("SESSION","TEST_NUMBER" ,0);
			fileNumber++; 
			iniWriterSessionConf.WriteInteger("SESSION","TEST_NUMBER",fileNumber);
			// write filename with path 
			ss.str("");
			ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\" << fileNumber << iniReaderSetup.ReadString("CHRONO","FILE_EXTENSION",".DTA");
			s  = ss.str();
			iniWriterSessionCTRL.WriteString("CHRONO", "FILENAME", (char*) s.c_str());
			
			iniWriterSessionCTRL.WriteString("CHRONO","SETUP_FILE",iniReaderSessionConf.ReadString("CHRONO", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CHRONO","SETUP_NAME",iniReaderSessionConf.ReadString("CHRONO", "SETUP_NAME","CHRONO"));
			iniWriterSessionCTRL.WriteString("CHRONO","TIME",iniReaderSessionConf.ReadString("CHRONO","TIME","300"));
			
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "CHRONO"); // will be cleared by explain script
			
			msg ="chrono, Mode ";
			msg.append("gamry, Session ");
			msg.appendInt(sessionNum);
			msg.append(", File ");
			msg.appendInt(fileNumber);
			//msg.append(thisUserName);
			msg.append(", Totaltime ");
			msg.appendInt(iniReaderSessionConf.ReadInteger("CHRONO","TIME", 300)); // default time 300 sec if nothing specifired 
			writeLogFile( thisUserName , sessionNum, msg);	
			return msg;
			//"Impedance, Mode ".get_mode().", Session $session, File $file, Totaltime $totaltime";

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username  or session number";
}


CkString gamryControl::cyclicVoltammetry(const char* thisUserName , unsigned int sessionNum)
{
	
	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			unsigned int fileNumber = (unsigned int) iniReaderSessionConf.ReadInteger("SESSION","TEST_NUMBER" ,0);
			fileNumber++; 
			iniWriterSessionConf.WriteInteger("SESSION","TEST_NUMBER",fileNumber);
			// write filename with path 
			ss.str("");
			ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\" << fileNumber << iniReaderSetup.ReadString("CYCLIC_VOLTAMMETRY","FILE_EXTENSION",".DTA");
			s  = ss.str();
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY", "FILENAME", (char*) s.c_str());
			
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY","SETUP_FILE",iniReaderSessionConf.ReadString("CYCLIC_VOLTAMMETRY", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY","SETUP_NAME",iniReaderSessionConf.ReadString("CYCLIC_VOLTAMMETRY", "SETUP_NAME","CYCLIC_VOLTAMMETRY"));
			iniWriterSessionCTRL.WriteString("CYCLIC_VOLTAMMETRY","TIME",iniReaderSessionConf.ReadString("CYCLIC_VOLTAMMETRY","TIME","300"));
			
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "CYCLIC_VOLTAMMETRY"); // will be cleared by explain script
			
			msg ="CYCLIC_VOLTAMMETRY, Mode ";
			msg.append("gamry, Session ");
			msg.appendInt(sessionNum);
			msg.append(", File ");
			msg.appendInt(fileNumber);
			//msg.append(thisUserName);
			msg.append(", Totaltime ");
			msg.appendInt(iniReaderSessionConf.ReadInteger("CYCLIC_VOLTAMMETRY","TIME", 300)); // default time 300 sec if nothing specifired 
			writeLogFile( thisUserName , sessionNum, msg);	
			return msg;
			//"Impedance, Mode ".get_mode().", Session $session, File $file, Totaltime $totaltime";

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username  or session number";
}



CkString gamryControl::setCurrent(const char* thisUserName , unsigned int sessionNum, const char* setPoint)
{

	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			
			// check if setPoint is a number else return  msg set point not a number 

			CkString newSetPoint ;
			newSetPoint = setPoint;
			/// check for is Number not done !!! 
			iniWriterSessionCTRL.WriteString("CURRENT","SETUP_FILE",iniReaderSessionConf.ReadString("CURRENT", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CURRENT","SETUP_NAME",iniReaderSessionConf.ReadString("CURRENT", "SETUP_NAME","CURRENT"));
			iniWriterSessionCTRL.WriteString("CURRENT","SET_POINT",(char*) setPoint);
			
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "SET_CURRENT"); // will be cleared by explain script
			
			msg ="new Current set point: ";
			msg.append(setPoint);
			writeLogFile( thisUserName , sessionNum, msg);		
			return msg;
			

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username  or session number";
}



CkString gamryControl::setVoltage(const char* thisUserName , unsigned int sessionNum, const char* setPoint)
{

	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			
			// check if setPoint is a number else return  msg set point not a number 

			CkString newSetPoint ;
			newSetPoint = setPoint;
			/// check for is Number not done !!! 
			iniWriterSessionCTRL.WriteString("VOLTAGE","SETUP_FILE",iniReaderSessionConf.ReadString("VOLTAGE", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("VOLTAGE","SETUP_NAME",iniReaderSessionConf.ReadString("VOLTAGE", "SETUP_NAME","VOLTAGE"));
			iniWriterSessionCTRL.WriteString("VOLTAGE","SET_POINT",(char*) setPoint);
			
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "SET_VOLTAGE"); // will be cleared by explain script
			
			msg ="new Voltage set point: ";
			msg.append(setPoint);
			writeLogFile( thisUserName , sessionNum, msg);	
			return msg;
			

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username  or session number";
}


CkString gamryControl::measureCurrent(const char* thisUserName , unsigned int sessionNum)
{

	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			
			

			iniWriterSessionCTRL.WriteString("CURRENT","SETUP_FILE",iniReaderSessionConf.ReadString("CURRENT", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("CURRENT","SETUP_NAME",iniReaderSessionConf.ReadString("CURRENT", "SETUP_NAME","CURRENT"));
			iniWriterSessionCTRL.WriteString("CURRENT","MEASURING","YES");
			
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "MEASURE_VI"); // will be cleared by explain script
			
			CkString measuringState;
			
			// wait for measurment to finish for max 2 sec 
			//loop
			for(int i=0; i <= 19; i++)
			{
				measuringState = iniReaderSessionCTRL.ReadString("CURRENT","MEASURING", "YES") ;
				if( measuringState.containsSubstring("NO"))
				{
					i = 20;
				}
				else Sleep(100);  // sleep 100ms 
			}


			if( measuringState.containsSubstring("NO"))	{
				// return measured value
				msg = "Current=";
				msg.append(iniReaderSessionCTRL.ReadString("CURRENT","MEASURED", "0.0")); 
			}
			else {
				// perhaps clear MEASURRING flag =  set "NO"
				// and send string fail to read voltage what ever  

			msg ="error could not measure";
			}
				
			return msg;
			

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username  or session number";
}


CkString gamryControl::measureVoltage(const char* thisUserName , unsigned int sessionNum)
{

	CkString serverState;
	CkString msg;
	serverState = UserINIRead.ReadString("SYSTEM","STATE","tag not found");
	if(serverState.containsSubstring("BUSY")) {
		msg = "msg server busy";
		return msg;
	}
	// check if user name and session number excist  else return false msg
	if(isValidSession(thisUserName,sessionNum)){
		// tell explain scipt what user is active  explsin script should read this tag all the time
		
		
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\session_control.INI" ;
		std::string s  = ss.str();
		CIniReader iniReaderSessionCTRL((char *)s.c_str());
		CIniWriter iniWriterSessionCTRL((char *)s.c_str());
		
		UserINIWrite.WriteString("USER","ACTIVE_SESSIONCTRL",(char*) s.c_str()); //
		UserINIWrite.WriteString("USER","ACTIVE_USER",(char*) thisUserName);
		
		ss.str("");
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\sessionconf.ini ";
		s  = ss.str();
		CIniReader iniReaderSessionConf((char *)s.c_str());
		CIniWriter iniWriterSessionConf((char *)s.c_str());
		CkString sessionState;
		sessionState = (const char *) iniReaderSessionCTRL.ReadString("SYS", "STATE","no tag found");
		if(sessionState.containsSubstring("IDLE"))
		{
			
			

			iniWriterSessionCTRL.WriteString("VOLTAGE","SETUP_FILE",iniReaderSessionConf.ReadString("VOLTAGE", "SETUP_FILE","EIS300.SET"));
			iniWriterSessionCTRL.WriteString("VOLTAGE","SETUP_NAME",iniReaderSessionConf.ReadString("VOLTAGE", "SETUP_NAME","VOLTAGE"));
			iniWriterSessionCTRL.WriteString("VOLTAGE","MEASURING","YES");
			
			UserINIWrite.WriteString("SYS", "STATE", "BUSY");		// will be cleared by explain script
			iniWriterSessionCTRL.WriteString("SYS", "STATE", "MEASURE_VI"); // will be cleared by explain script
			
			CkString measuringState;
			
			// wait for measurment to finish for max 2 sec 
			//loop
			for(int i=0; i <= 19; i++)
			{
				measuringState = iniReaderSessionCTRL.ReadString("VOLTAGE","MEASURING", "YES") ;
				if( measuringState.containsSubstring("NO"))
				{
					
					i = 20;
				}
				else Sleep(100);  // sleep 100ms

				
			}


			if( measuringState.containsSubstring("NO"))	{
				// return measured value
				msg = "Voltage=";
				msg.append(iniReaderSessionCTRL.ReadString("VOLTAGE","MEASURED", "0.0")); 
			}
			else {
				// perhaps clear MEASURRING flag =  set "NO"
				// and send string fail to read voltage what ever  

			msg ="error could not measure";
			}
				
			return msg;
			

		}
		else 	return msg = "server session doing something else";
		
	}
	return msg = "invalid username  or session number";
}



bool gamryControl::writeLogFile(const char* thisUserName , unsigned int sessionNum, CkString newLine )
{

	// check if user name and session number excist  else return false 
	if(isValidSession(thisUserName,sessionNum)){
		// make file path and grap file  and add line to end of file
		//
		// load logfile 
		// append newLine 
		// save newLogfile
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\logfile.txt" ;
		std::string s  = ss.str();
		CkString logfile;
		logfile.loadFile(s.c_str(),"us-ascii");
		// append time stamp bhefore line 
		logfile.appendCurrentDateRfc822();
		logfile.append("  ");
		logfile.appendStr(newLine); 
		// check if ends with newline charter else add  
		if(! newLine.endsWith("\r\n"))
		{
			logfile.append("\r\n");
		}
		logfile.saveToFile(s.c_str(),"us-ascii");
		
		return true; 
	}
	else return false; 
}


CkString gamryControl::readLogFile(const char* thisUserName , unsigned int sessionNum)
{
	CkString logfile;

	if(isValidSession(thisUserName,sessionNum)){
		std::stringstream ss;
		ss << homeFolder.getString() << "\\users\\" << thisUserName << "\\" << sessionNum << "\\logfile.txt" ;
		std::string s  = ss.str();
		logfile.loadFile(s.c_str(),"us-ascii");
		
	}
	else logfile = " no logfile for this user and session found \n";
	
	return logfile;
}