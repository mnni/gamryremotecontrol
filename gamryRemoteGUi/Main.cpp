//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: main.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

// CppWin32Dialog.cpp : Defines the entry point for the application.
//
 
#include <windows.h>
#include "common.h"
#include "gamrycontrol.h"
#include "server.h"
//#include "frmConf.h"
#include "resource.h" 
#include <process.h>     // needed for _beginthread()</span>
#include <io.h> 
#include <stdlib.h>
#include <fcntl.h>
#include <iostream>
#include <ios>
#include "prsht.h"
#include "frmConf.h"
//#include "Commctrl.h"
#include "Shlwapi.h" 


// For nice controls look
#define MAX_LOADSTRING 100
bool IgnoreEnChange = false;
HINSTANCE hInstanceGlobal;
gamryControl *GamryPtrInterface; 
bool isUsrLogIn = false; 
 
INT_PTR CALLBACK DialogProc(HWND ,UINT ,WPARAM ,LPARAM);
INT_PTR CALLBACK HdCreateUserFrame(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdLogInFrame(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdselectSession(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdConf(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdManuelEditEIS300(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdManuelEditsessionConf(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdSelectEISMode(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdSelectChronoMode(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK HdSetCurrent(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK modEditSetCurrentProc(HWND hWnd, UINT msg, WPARAM wParam,LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData);
INT_PTR CALLBACK HdSetVoltage(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

ATOM MyRegisterClass(HINSTANCE hInstance);

void updateIPfeelt(HWND hDlg);
void updateMainFrame(HWND hDlg);
DWORD WINAPI startServerThread(LPVOID lpParam);
static void OpenConsole();
void AppendTextToLogView( const HWND &hwnd, TCHAR *newText );
void RedirectStdoutThreadRun();
void updateLogView(const HWND &hwnd );
///////// buffer for logwriting
static FILE* NewSTDOutput;
	
/// for capture key in the setPoint edit box

WNDPROC wpOrigEditProcSetCurrent; 
	
//////////////////////////////

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine,  int nCmdShow)
{
//OpenConsole();

	AllocConsole();
	freopen("CONOUT$", "w+t", stdout);

CHAR pBuf[MAX_PATH];
int bytes = GetModuleFileNameA(NULL, pBuf, MAX_PATH);
char *lpStr1;
lpStr1 = pBuf;
PathRemoveFileSpec(lpStr1);
std::cout << lpStr1 << std::endl;
 
std::cout << "test if something is going out  to the world" << std::endl;

HWND hDlg;
	  	
 MSG msg;
 BOOL ret;
 hInstanceGlobal = hInstance;
 //InitCommonControls();
  
 
 //gamryControl CTRLGAMRY("c:\\GamryRemoteCTRL");
 gamryControl CTRLGAMRY(lpStr1);
 
 GamryPtrInterface = &CTRLGAMRY;
 hDlg = CreateDialogParam(hInstanceGlobal, MAKEINTRESOURCE(frmMain), 0, DialogProc, 0);
 DWORD myThreadID;
 HANDLE myHandle = CreateThread(NULL, 0, startServerThread, &CTRLGAMRY, 0, &myThreadID);

 updateIPfeelt(hDlg);
 // Main message loop:
 while((ret = GetMessage(&msg, 0, 0, 0)) != 0)
 {
    if(ret == -1)
      return -1;
 
    if(!IsDialogMessage(hDlg, &msg))
 {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }
 
 return (int) msg.wParam;
}



INT_PTR CALLBACK DialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
 int wmId, wmEvent;
 PAINTSTRUCT ps;
 HDC hdc;
 UNREFERENCED_PARAMETER(lParam);
  
 
 switch (message)
 {
 case WM_INITDIALOG:
	 
	 
  return (INT_PTR)TRUE;
 case WM_COPYDATA:
      {
        PCOPYDATASTRUCT pMyCDS = (PCOPYDATASTRUCT) lParam;
        LPCSTR szString = (LPCSTR)(pMyCDS->lpData);
        //AppendTextToLogView(hDlg, szString);
      }
      break;

 case WM_COMMAND:
  wmId    = LOWORD(wParam);
  wmEvent = HIWORD(wParam);
  // Parse the menu selections:
  switch (wmId)
  {
    case ID_FILE_EXIT:
		PostQuitMessage(0);
    break;
	case ID_FILE_CREATENEWUSER:
			DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmCreateUser), hDlg, HdCreateUserFrame);
			updateMainFrame(hDlg);
			
	break;
	case ID_FILE_LOGIN:
				// log in
			DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmLogIn), hDlg, HdLogInFrame);
			updateMainFrame(hDlg);
			
			

	break;
	case ID_FILE_SwitchUser:
				// log in
			DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmLogIn), hDlg, HdLogInFrame);
			updateMainFrame(hDlg);
			
	break;
	case ID_USERCONTROL_CREATENEWSESSION:
		{
		unsigned int newSessionNum = GamryPtrInterface->incrSessionNum(); 
		GamryPtrInterface->setSessionNum(newSessionNum); 
		updateMainFrame(hDlg);
		
		break;
		}
	
	case ID_USERCONTROL_SELECTSESSION:
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmSelectSession), hDlg, HdselectSession);
		updateMainFrame(hDlg);
		
	break;
	case ID_USERCONTROL_CONFIGCURRENTSESSION:
			//DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmConfigSession), hDlg, HdConf);	
		configPage(hInstanceGlobal);
	break;
	case ID_CONFIGCURRENTSESSION_MANUELEDITEIS300SETUPFILE:
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmManualEditEIS300), hDlg, HdManuelEditEIS300);
		updateMainFrame(hDlg);
		
	break;
	case ID_CONFIGCURRENTSESSION_MANUELEDIT_sessionControl:
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmManualEdit_SessionConf), hDlg, HdManuelEditsessionConf);
		updateMainFrame(hDlg);
		
	break;
	case ID_CONFIGCURRENTSESSION_CONFIGEIS:
		// write to Frame work that it should start conf window 
		// start menu and ask if POT or GAL mode   then run framework config menu Item
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmEISMode), hDlg, HdSelectEISMode);
				
	break;
	case ID_CONFIGCURRENTSESSION_CONFIGCHRONO:
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmCHronoMode), hDlg, HdSelectChronoMode);
	break;
	case ID_CONFIGCURRENTSESSION_CYCLICVOLTAMMETRY:
		 std::cout << GamryPtrInterface->openFrameworkConfMenu(GamryPtrInterface->getCurrentUserName().getString(), GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName().getString()), "CONF_CYCLIC_VOLTAMMETRY") << std::endl;
	break;
	case ID_STARTMEASUREMENT_EIS:
		GamryPtrInterface->eis(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()));
		// perhaps write out feedback in terminal or window 
	break;
	case ID_STARTMEASUREMENT_CHRONO:
		GamryPtrInterface->chrono(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()));
		// perhaps write out feedback in terminal or window 	
	break;
	case ID_STARTMEASUREMENT_CYCLICVOLTAMMETRY:
		GamryPtrInterface->cyclicVoltammetry(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()));
	break;
	case ID_STARTMEASUREMENT_SETCURRENT:
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmSetCurrent), hDlg, HdSetCurrent);
	break; 
	case ID_STARTMEASUREMENT_SETVOLTAGE:
		DialogBox(hInstanceGlobal, MAKEINTRESOURCE(frmSetVoltage), hDlg, HdSetVoltage);
	break; 
	case ID_HELP_STARTDEBUGCONSOL:
		OpenConsole(); 
	break;

  }
  break;
  case WM_PAINT:
  hdc = BeginPaint(hDlg, &ps);
  updateMainFrame(hDlg);

  //updateMainFrame(hDlg);
  // TODO: Add any drawing code here...
  EndPaint(hDlg, &ps);
  break;
 case WM_DESTROY:
  PostQuitMessage(0);
  break;
 case WM_CLOSE:
  PostQuitMessage(0);
 break;
 
 }
 return (INT_PTR)FALSE;
}



// Message handler for HdCreateUserFrame box.
INT_PTR CALLBACK HdCreateUserFrame(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK_frmCREATEUSER )
		{
			// get userName 
			// check if valid and create user
			// if valid return and update user and session felt in mainFrame 
			// if not valid pop up a box writing fail msg or just write in a msg felt in create user frame
				int len = GetWindowTextLength(GetDlgItem(hDlg, IDC_Input_newUser));
						if(len > 0)
						{
							
							char* buf;

							buf = (char*)GlobalAlloc(GPTR, len + 1);
							GetDlgItemText(hDlg, IDC_Input_newUser, buf, len + 1);

							bool ValidUserName = GamryPtrInterface->createNewUser(buf);
							if(!ValidUserName){
								// user could not be created 
								SetDlgItemText(hDlg, IDC_frmCreateUserFailMsg, "! UserName incorrect or in use");
								
							}
							else{// susses end dialog box  and update main frame with user name etc.
								EndDialog(hDlg, LOWORD(wParam));

							}
						
						}
						else{
							SetDlgItemText(hDlg, IDC_frmCreateUserFailMsg, "!!! UserName incorrect please type a userName or select cancel");
						}
			

			
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL_frmCREATEUSER){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
		break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}


// Message handler for login box.
INT_PTR CALLBACK HdLogInFrame(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK_frmLogIn )
		{
			// get userName 
			// check if valid and create user
			// if valid return and update user and session felt in mainFrame 
			// if not valid pop up a box writing fail msg or just write in a msg felt in create user frame
				int len = GetWindowTextLength(GetDlgItem(hDlg, IDC_frmLogin_UserName));
						if(len > 0)
						{
							
							char* buf;

							buf = (char*)GlobalAlloc(GPTR, len + 1);
							GetDlgItemText(hDlg, IDC_frmLogin_UserName, buf, len + 1);

							bool ValidUserName = GamryPtrInterface->setUser(buf);
							if(!ValidUserName){
								// user do not exist 
								SetDlgItemText(hDlg, IDC_frmLogIn_FailMsg, "! UserName incorrect please type in correct user name or create new user");
								
							}
							else{// susses end dialog box  and update main frame with user name etc.
								isUsrLogIn = true;
								EndDialog(hDlg, LOWORD(wParam));

							}
						
						}
						else{
							SetDlgItemText(hDlg, IDC_frmLogIn_FailMsg, "!!! UserName incorrect please type a  valid user Name of minimum one charetar");
						}
			

			
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL_frmLogIn){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}




INT_PTR CALLBACK HdselectSession(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{
		// creat session list or perhaps up in WM_INITDIALOG:
		
			int NumSession = GamryPtrInterface->getTotalNumSession(GamryPtrInterface->getCurrentUserName().getString());
			//std::cout << "num of session:" << NumSession << std::endl; 
			for(int i = 1;  i <= NumSession; i++){
			CkString str;
			str.appendInt(i);
			SendDlgItemMessage(hDlg, IDC_frmSessionListBox, LB_ADDSTRING, 0, (LPARAM) str.getString());
			
			}
		//int index = SendDlgItemMessage(hDlg, IDC_frmSessionListBox, LB_ADDSTRING, 0, (LPARAM)"1"); 
		
		//return (INT_PTR)TRUE;
		break;
		}
	
	case WM_COMMAND:

		if( LOWORD(wParam) ==  IDC_ListBox_frmSession)
		{
			
			if( HIWORD(wParam) == LBN_SELCHANGE)
			{
			
							
				HWND hList = GetDlgItem(hDlg, IDC_frmSessionListBox);
				int index = SendMessage(hList, LB_GETCURSEL, 0, 0) + 1;
				CkString str;
				str.append("Selected Session: ");
				str.appendInt(index);
				//std::cout << str<< std::endl;
				SetDlgItemText(hDlg, ID_TEXT_Session_frmSelectSession, str.getString());

			}
		}
		else if (LOWORD(wParam) == IDOK_frmSelectSession )
		{
			// check if any session is selected else post no selected session
			HWND hList = GetDlgItem(hDlg, IDC_frmSessionListBox);	
			int selSession = SendMessage(hList, LB_GETCURSEL, 0, 0) ;
			if(selSession == LB_ERR){
				SetDlgItemText(hDlg, ID_TEXT_Session_frmSelectSession, "! no session selected");
				return (INT_PTR)TRUE;
			}
			else{
				// set session number 
				if(GamryPtrInterface->setSessionNum((unsigned int) selSession + 1))
				EndDialog(hDlg, LOWORD(wParam));
				else SetDlgItemText(hDlg, ID_TEXT_Session_frmSelectSession, "selection error !!!");
			}
			
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL_frmLogIn){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}



// Message handler for login box.
INT_PTR CALLBACK HdConf(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK_frmLogIn )
		{
			
			
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL_frmLogIn){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK HdManuelEditEIS300(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{

			CkString fileContens;
			fileContens = GamryPtrInterface->loadRawFile_EIS300(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName())); 
			HWND hEdit = GetDlgItem(hDlg, IDC_EDIT1_frmManualEditEis300);
			SetWindowText(hEdit, (LPCSTR) fileContens.getString());
		
		
		return (INT_PTR)TRUE;
		}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK )
		{
			
			CkString fileContens;
			HWND hEdit = GetDlgItem(hDlg, IDC_EDIT1_frmManualEditEis300);
			int ntextLenght = GetWindowTextLength(hEdit) +1;
			char* buf;
			buf = (char*)GlobalAlloc(GPTR, ntextLenght);
			GetWindowText(hEdit,buf ,ntextLenght) ;
			fileContens = buf; 
			if(GamryPtrInterface->saveFileRaw_EIS300(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()),fileContens))
			{
				EndDialog(hDlg, LOWORD(wParam));
			}
			

			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK HdManuelEditsessionConf(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{

			CkString fileContens;
			fileContens = GamryPtrInterface->loadRawFile_sessionConf(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName())); 
			HWND hEdit = GetDlgItem(hDlg, IDC_EDIT_frmManualEditSessionConf);
			SetWindowText(hEdit, (LPCSTR) fileContens.getString());
		
		
		return (INT_PTR)TRUE;
		}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK )
		{
			
			CkString fileContens;
			HWND hEdit = GetDlgItem(hDlg, IDC_EDIT_frmManualEditSessionConf);
			int ntextLenght = GetWindowTextLength(hEdit) +1;
			char* buf;
			buf = (char*)GlobalAlloc(GPTR, ntextLenght);
			GetWindowText(hEdit,buf ,ntextLenght) ;
			fileContens = buf; 
			if(GamryPtrInterface->saveFileRaw_sessionConf(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()),fileContens))
			{
				EndDialog(hDlg, LOWORD(wParam));
			}
			

			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK HdSelectEISMode(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{
			// load last setting  from conf file  if POT select Ratio button POT and vise vase 
			// perhaps group the 2 ratio buttons together 
			//if Pot  selected 
			if(GamryPtrInterface->readSessionConfFile(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()), "EIS", "MODE").containsSubstring("POT")){

				CheckRadioButton(hDlg,IDC_RADIO_POT,IDC_RADIO_GAL,IDC_RADIO_POT);
					
			}
			else CheckRadioButton(hDlg, IDC_RADIO_POT , IDC_RADIO_GAL,IDC_RADIO_GAL);

			return (INT_PTR)TRUE;
		}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK )
		{
			
			// get state of the ratio button  and  safe the setting to conf file
			// close window 
			 if (IsDlgButtonChecked(hDlg, IDC_RADIO_POT)){
				 // pot selected  write 
				 //std::cout << " POT mode selected" << std::endl;
				 GamryPtrInterface->configSessionConfFile(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()),"EIS","MODE","POT");
				std::cout << GamryPtrInterface->openFrameworkConfMenu(GamryPtrInterface->getCurrentUserName().getString(), GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName().getString()), "CONF_EIS_POT") << std::endl;
			 }
			 else{
				GamryPtrInterface->configSessionConfFile(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()),"EIS","MODE","GAL");
				std::cout << GamryPtrInterface->openFrameworkConfMenu(GamryPtrInterface->getCurrentUserName().getString(), GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName().getString()), "CONF_EIS_GAL") << std::endl;	
			 }
			
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK HdSelectChronoMode(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{
			// load last setting  from conf file  if POT select Ratio button POT and vise vase 
			// perhaps group the 2 ratio buttons together 
			//if Pot  selected 
			if(GamryPtrInterface->readSessionConfFile(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()), "CHRONO", "MODE").containsSubstring("AMP")){

				CheckRadioButton(hDlg,IDC_RADIO_ChronoPot,IDC_RADIO_CHRONO_AMP,IDC_RADIO_CHRONO_AMP);
					
			}
			else CheckRadioButton(hDlg, IDC_RADIO_ChronoPot , IDC_RADIO_CHRONO_AMP, IDC_RADIO_ChronoPot);

			return (INT_PTR)TRUE;
		}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK )
		{
			
			// get state of the ratio button  and  safe the setting to conf file
			// close window 
			 if (IsDlgButtonChecked(hDlg, IDC_RADIO_ChronoPot)){
				 // pot selected  write 
				 //std::cout << " POT mode selected" << std::endl;
				 GamryPtrInterface->configSessionConfFile(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()),"CHRONO","MODE","POT");
				std::cout << GamryPtrInterface->openFrameworkConfMenu(GamryPtrInterface->getCurrentUserName().getString(), GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName().getString()), "CONF_CHRONOPOT") << std::endl;
			 }
			 else{
				GamryPtrInterface->configSessionConfFile(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()),"CHRONO","MODE","AMP");
				std::cout << GamryPtrInterface->openFrameworkConfMenu(GamryPtrInterface->getCurrentUserName().getString(), GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName().getString()), "CONF_CHRONOAMP") << std::endl;	
			 }
			
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	}
	return (INT_PTR)FALSE;
}


INT_PTR CALLBACK HdSetCurrent(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{
			//HWND editSetCurrent = GetDlgItem(hDlg, IDC_EDIT_SET_CURRENT);
			////SetWindowSubclass(editSetpoint, OwnerSetPointProc, 0, 0);
			//wpOrigEditProcSetCurrent =(WNDPROC)GetWindowLongPtr(editSetCurrent, GWLP_WNDPROC);//get the default procedure (WNDPROC) SetWindowLong(editSetCurrent,GWL_WNDPROC, (LONG) OwnerSetCurrentProc); 
   			//SetWindowLongPtr(editSetCurrent, GWLP_WNDPROC, (LONG_PTR)modEditSetCurrentProc);//set your custom procedure :)
			return (INT_PTR)TRUE;
		}
	break;
	case WM_DESTROY:
			// Remove the subclass from the edit control.
			//SetWindowLong(editSetCurrent, GWL_WNDPROC,(LONG) wpOrigEditProcSetCurrent); 
	break;
	case WM_COMMAND:
		/*
		if (HIWORD( wParam ) == EN_CHANGE) 
		{
			if (IgnoreEnChange == true){
				IgnoreEnChange = false; 
			}
			else if( LOWORD(wParam) == IDC_EDIT_SET_CURRENT)
			{
				// get last  char of IDC_EDIT_SET_CURRENT
				// check for valid charter  and delete or return 0 if not valid		
				static int counter = 0; 

				int len = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT_SET_CURRENT));
				char* buf;
				buf = (char*)GlobalAlloc(GPTR, len + 1);
				GetDlgItemText(hDlg, IDC_EDIT_SET_CURRENT, buf, len + 1);
				std::cout << buf[len-1] << std::endl; 

				CkString input ;
				input = buf;
				
				for( int i = 0; i < len-1; i++ ){
					
				  if(!    ((input.charAt(i) >= '0' && input.charAt(i) <= '9')
					 || buf[len-1] == '.'))
					 {
						input.removeChunk(i,1);	
						IgnoreEnChange = true;
						SetDlgItemText(hDlg,IDC_EDIT_SET_CURRENT,input.getString());
					}
				}
				
			}
		}*/
		if (LOWORD(wParam) == IDOK )
		{
		
			int len = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT_SET_CURRENT));
			if(len > 0)
			{
				char* buf;
				buf = (char*)GlobalAlloc(GPTR, len + 1);
				GetDlgItemText(hDlg, IDC_EDIT_SET_CURRENT, buf, len + 1);
				/// think if should take comma numbers 
				GamryPtrInterface->setCurrent(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()), buf );

				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;

			}



			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	
	}
	return (INT_PTR)FALSE;
	UNREFERENCED_PARAMETER(lParam); 
}


LRESULT CALLBACK modEditSetCurrentProc(HWND hWnd, UINT msg, WPARAM wParam,LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
{
	
	
   if (msg == WM_KEYDOWN) std::cout << " key down " << std::endl;

	if(msg == WM_CHAR)
    {
		std::cout << "message= "<< msg << " wparam= " << wParam << std::endl; 
        // Make sure we only allow specific characters
        if(!    ((wParam >= '0' && wParam <= '9')
                || wParam == '.'
                || wParam == VK_RETURN
                || wParam == VK_DELETE
                || wParam == VK_BACK))
        {
            return 0;
        }
    }
return CallWindowProc(wpOrigEditProcSetCurrent, hWnd, msg,wParam, lParam); 
}


INT_PTR CALLBACK HdSetVoltage(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_DESTROY:
			// Remove the subclass from the edit control.
			//SetWindowLong(editSetCurrent, GWL_WNDPROC,(LONG) wpOrigEditProcSetCurrent); 
	break;
	case WM_COMMAND:
		
		if (LOWORD(wParam) == IDOK )
		{
		
			int len = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT_SET_VOLTAGE));
			if(len > 0)
			{
				char* buf;
				buf = (char*)GlobalAlloc(GPTR, len + 1);
				GetDlgItemText(hDlg, IDC_EDIT_SET_VOLTAGE, buf, len + 1);
				/// think if should take comma numbers 
				GamryPtrInterface->setVoltage(GamryPtrInterface->getCurrentUserName(),GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()), buf );

				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;

			}



			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if(LOWORD(wParam) == IDCANCEL){
			// do nothing 
				EndDialog(hDlg, LOWORD(wParam));
		}
	break;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
	break;
	
	}
	return (INT_PTR)FALSE;
	UNREFERENCED_PARAMETER(lParam); 
}


void updateIPfeelt(HWND hDlg)
{
	CkString ethSetup ; 

ethSetup.append( GamryPtrInterface->getEthIP().getString());
ethSetup.prepend("IP: ");
ethSetup.append("      Port: ");
ethSetup.appendInt(GamryPtrInterface->getEthPort());


SetDlgItemText(hDlg, IDC_IP_SHOW,(LPCSTR) ethSetup.getString());


}

void updateMainFrame(HWND hDlg)
{
	// redirect stdout  etc. and show to log window 
	//AppendTextToLogView( hDlg, NewSTDOutput->  );
	char buffer [1000];

	if (NewSTDOutput == NULL) perror ("Error opening file");
   else
   {
     while ( ! feof (NewSTDOutput) )
     {
       if ( fgets (buffer , 1000 , NewSTDOutput) == NULL ) break;
       fputs (buffer , stdout);
     }
     //fclose (pFile); // flush maybe 
   }
	//AppendTextToLogView( hDlg, "test 2\n" );
	CkString buffStd;
	buffStd.append(buffer);
	buffStd.toCRLF();
	TCHAR newStr = (TCHAR) buffStd.getString();
	//AppendTextToLogView( hDlg, &newStr);


updateIPfeelt(hDlg);

CkString userName;
userName = GamryPtrInterface->getCurrentUserName();
unsigned int sessionNum = GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName());
CkString sessionNumStr;
sessionNumStr.appendInt(sessionNum);

if(isUsrLogIn){
	SetDlgItemText(hDlg, IDC_USERNAME, userName.getString());
	
	SetDlgItemText(hDlg, IDC_SESSION_NUM, sessionNumStr.getString());
	// if a user is selected enable menu'ss 
	
	HMENU hmenu = GetMenu(hDlg);
	EnableMenuItem(hmenu,ID_FILE_SwitchUser,MF_ENABLED);
	EnableMenuItem(hmenu,ID_USERCONTROL_CREATENEWSESSION,MF_ENABLED);
	EnableMenuItem(hmenu,ID_USERCONTROL_SELECTSESSION,MF_ENABLED);
	EnableMenuItem(hmenu,ID_CONFIGCURRENTSESSION_MANUELEDIT_sessionControl,MF_ENABLED);
	EnableMenuItem(hmenu,ID_CONFIGCURRENTSESSION_MANUELEDITEIS300SETUPFILE,MF_ENABLED);
	EnableMenuItem(hmenu,ID_CONFIGCURRENTSESSION_CONFIGEIS,MF_ENABLED);
	EnableMenuItem(hmenu,ID_CONFIGCURRENTSESSION_CONFIGCHRONO,MF_ENABLED);
	EnableMenuItem(hmenu,ID_CONFIGCURRENTSESSION_CYCLICVOLTAMMETRY,MF_ENABLED);
		//EnableMenuItem(hmenu,ID_CONFIGCURRENTSESSION_REPEATINGCHRONOAMPEROMETRY,MF_ENABLED);
	
			
	EnableMenuItem(hmenu,ID_STARTMEASUREMENT_EIS,MF_ENABLED);
	EnableMenuItem(hmenu,ID_STARTMEASUREMENT_CHRONO,MF_ENABLED);
	EnableMenuItem(hmenu,ID_STARTMEASUREMENT_CYCLICVOLTAMMETRY,MF_ENABLED);
	EnableMenuItem(hmenu,ID_STARTMEASUREMENT_CHRONOPOTENTIOMETRY,MF_ENABLED);
	EnableMenuItem(hmenu,ID_STARTMEASUREMENT_SETCURRENT,MF_ENABLED);
	EnableMenuItem(hmenu,ID_STARTMEASUREMENT_SETVOLTAGE,MF_ENABLED);
	

	EnableMenuItem(hmenu,ID_FILE_LOGIN,MF_DISABLED);
	
		
// perhaps disable login menu item 
	}
updateLogView(hDlg);
}

DWORD WINAPI startServerThread(LPVOID lpParam)
{
	remoteGamryServer server;
	//gamryControl &CTRLGAMRY =  *((gamryControl*) lpParam);
	
	server.startServer((gamryControl*) lpParam); 
	return 0; 
}

static void OpenConsole()
{
    int outHandle, errHandle, inHandle;
    FILE *outFile, *errFile, *inFile;
    AllocConsole();
    CONSOLE_SCREEN_BUFFER_INFO coninfo;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
    coninfo.dwSize.Y = 9999;
    SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

    outHandle = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
    errHandle = _open_osfhandle((long)GetStdHandle(STD_ERROR_HANDLE),_O_TEXT);
    inHandle = _open_osfhandle((long)GetStdHandle(STD_INPUT_HANDLE),_O_TEXT );

    outFile = _fdopen(outHandle, "w" );
    errFile = _fdopen(errHandle, "w");
    inFile =  _fdopen(inHandle, "r");

    *stdout = *outFile;
    *stderr = *errFile;
    *stdin = *inFile;

    setvbuf( stdout, NULL, _IONBF, 0 );
    setvbuf( stderr, NULL, _IONBF, 0 );
    setvbuf( stdin, NULL, _IONBF, 0 );

    std::ios::sync_with_stdio();
	NewSTDOutput = outFile;
}


void updateLogView(const HWND &hwnd )
{
	// get edit control from dialog
	CkString logfile;
	logfile = GamryPtrInterface->readLogFile(GamryPtrInterface->getCurrentUserName(), GamryPtrInterface->getCurrentSessionNum(GamryPtrInterface->getCurrentUserName()));
    HWND hwndOutput = GetDlgItem( hwnd, IDC_LOGVIEW );

	SetDlgItemText(hwnd,IDC_LOGVIEW,logfile.getString());


}


void AppendTextToLogView( const HWND &hwnd, TCHAR *newText )
{
    // get edit control from dialog
    HWND hwndOutput = GetDlgItem( hwnd, IDC_LOGVIEW );

    // get the current selection
    DWORD StartPos, EndPos;
    SendMessage( hwndOutput, EM_GETSEL, reinterpret_cast<WPARAM>(&StartPos), reinterpret_cast<WPARAM>(&EndPos) );

    // move the caret to the end of the text
    int outLength = GetWindowTextLength( hwndOutput );
    SendMessage( hwndOutput, EM_SETSEL, outLength, outLength );

    // insert the text at the new caret position
    SendMessage( hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(newText) );

    // restore the previous selection
    SendMessage( hwndOutput, EM_SETSEL, StartPos, EndPos );
}

