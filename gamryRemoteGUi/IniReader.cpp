//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: iniReader.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "IniReader.h"



CIniReader::CIniReader()
{
	//  do nothing for now 

}

CIniReader::CIniReader(char* szFileName)
{
 memset(m_szFileName, 0x00, 255);
 memcpy(m_szFileName, szFileName, strlen(szFileName));
 
}
int CIniReader::ReadInteger(char* szSection, char* szKey, int iDefaultValue)
{
 int iResult = GetPrivateProfileInt(szSection,  szKey, iDefaultValue, m_szFileName); 
 return iResult;
}
float CIniReader::ReadFloat(char* szSection, char* szKey, float fltDefaultValue)
{
 char szResult[255];
 char szDefault[255];
 float fltResult;
 sprintf_s(szDefault, "%f",fltDefaultValue);
 GetPrivateProfileString(szSection,  szKey, szDefault, szResult, 255, m_szFileName); 
 fltResult =  atof(szResult);
 return fltResult;
}
bool CIniReader::ReadBoolean(char* szSection, char* szKey, bool bolDefaultValue)
{
 char szResult[255];
 char szDefault[255];
 bool bolResult;
 sprintf_s(szDefault, "%s", bolDefaultValue? "True" : "False");
 GetPrivateProfileString(szSection, szKey, szDefault, szResult, 255, m_szFileName); 
 bolResult =  (strcmp(szResult, "True") == 0 || 
		strcmp(szResult, "true") == 0) ? true : false;
 return bolResult;
}
char* CIniReader::ReadString(char* szSection, char* szKey, const char* szDefaultValue)
{
 char* szResult = new char[255];
 memset(szResult, 0x00, 255);
 GetPrivateProfileString(szSection,  szKey, 
		szDefaultValue, szResult, 255, m_szFileName); 
 return szResult;
}





bool CIniReader::sectionExists(CkString strSection)
{
	char ac_Result[100];	
	// Get the info from the .ini file	
	m_lRetValue = GetPrivateProfileString(strSection,NULL,	"",ac_Result, 90, m_szFileName);
	// Return if we could retrieve any info from that section
	return (m_lRetValue > 0);
}



 void CIniReader::getSectionNames(CkStringArray* sectionList)  //returns collection of section names
{
	char ac_Result[2000];
	
	
	m_lRetValue = GetPrivateProfileSectionNames(ac_Result,2000,m_szFileName);
	
	CkString strSectionName;
	for(int i=0; i<m_lRetValue; i++)
	{
		if(ac_Result[i] != '\0') {
			strSectionName.appendChar(ac_Result[i]);
		} else {
			if(strSectionName != "") {
				sectionList->Append(strSectionName);
			}
			strSectionName = "";
		}
	}


}


 void CIniReader::getSectionData(CkStringArray* sectionDataList,CkString strSection)  
{
	char ac_Result[2000];  //change size depending on needs
	
	m_lRetValue = GetPrivateProfileSection(strSection, ac_Result, 2000, m_szFileName);

	CkString strSectionData;
	for(int i=0; i<m_lRetValue; i++)
	{
		if(ac_Result[i] != '\0') {
			strSectionData.appendChar( ac_Result[i]);
		} else {
			if(strSectionData != "") {
				sectionDataList->Append(strSectionData);
			}
			strSectionData = " ";
		}
	}
	
}
