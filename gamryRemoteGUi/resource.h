//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin Nørby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: resource.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////


//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by gamryRemoteGUi.rc
//
#define IDR_MENU_MAIN                   102
#define frmMain                         103
#define frmCreateUser                   105
#define frmLogIn                        106
#define frmConfigSession                107
#define frmSelectSession                111
#define IDD_PROPPAGE_MEDIUM             115
#define IDD_PROPPAGE_MEDIUM1            116
#define IDD_PROPPAGE_MEDIUM2            117
#define frmManualEditEIS300             118
#define frmManualEdit_SessionConf       119
#define frm_ConfEIS                     120
#define frmEISMode                      121
#define frmSetCurrent                   122
#define frmSetVoltage                   123
#define frmCHronoMode                   124
#define IDB_CLEAR_LOG                   1007
#define IDC_LOGVIEW                     1008
#define IDC_USERNAME                    1010
#define IDC_SESSION_NUM                 1011
#define IDC_Input_newUser               1012
#define IDOK_frmCREATEUSER              1014
#define IDCANCEL_frmCREATEUSER          1015
#define IDC_frmCreateUserFailMsg        1016
#define IDC_frmLogin_UserName           1017
#define IDOK_frmLogIn                   1018
#define IDCANCEL_frmLogIn               1019
#define IDC_frmLogIn_FailMsg            1020
#define IDC_frmSessionListBox           1022
#define IDC_ListBox_frmSession          1022
#define IDOK_frmSelectSession           1023
#define IDCANCEL_frmSelectSession       1024
#define id_listSession_frmSelectSession 1025
#define ID_TEXT_Session_frmSelectSession 1025
#define IDC_TAB1                        1026
#define IDC_EDIT1                       1027
#define IDC_EDIT_setupFile              1028
#define IDC_EDIT_SETUP_PROB             1029
#define IDC_EDIT1_frmManualEditEis300   1030
#define IDC_EDIT_frmManualEditSessionConf 1031
#define IDC_BUTTON_TAKETime             1034
#define IDC_EDIT2                       1035
#define IDC_IP_SHOW                     1036
#define IDC_RADIO_POT                   1038
#define IDC_RADIO_GAL                   1039
#define IDC_EDIT_SET_CURRENT            1040
#define IDC_EDIT_SET_VOLTAGE            1043
#define IDC_RADIO_ChronoPot             1044
#define IDC_RADIO_CHRONO_AMP            1045
#define ID_FILE_LOGOUT                  40002
#define ID_FILE_LOGIN                   40003
#define ID_USERCONTROL_CREATENEWSESSION 40004
#define ID_USERCONTROL_SELECTSESSION    40005
#define ID_USERCONTROL_CONFIGCURRENTSESSION 40006
#define ID_HELP_ABOUT                   40007
#define ID_FILE_EXIT                    40008
#define ID_FILE_CREATENEWUSER           40009
#define ID_FILE_SwitchUser              40010
#define ID_BUTTON2                      40011
#define ID_CONFIGCURRENTSESSION_MANUELEDITEIS300SETUPFILE 40015
#define ID_CONFIGCURRENTSESSION_MANUELEDITEIS300SETUPFILE40016 40016
#define ID_CONFIGCURRENTSESSION_MANUELEDIT 40017
#define ID_CONFIGCURRENTSESSION_MANUELEDIT_sessionControl 40018
#define ID_CONFIGCURRENTSESSION_CONFIGEIS 40019
#define ID_CONFIGEIS_F                  40020
#define ID_CONFIGEIS                    40021
#define ID_STARTMEASUREMENT_EIS         40022
#define ID_CONFIGCURRENTSESSION_CONFIGCHRONOAMPEROMETRY 40023
#define ID_STARTMEASUREMENT_CHRONOAMPEROMETRY 40024
#define ID_CONFIGCURRENTSESSION_CONFIGPOTENTIOSTATICSCAN 40025
#define ID_STARTMEASUREMENT_POTENTIOSTATICSCAN 40026
#define ID_STARTMEASUREMENT_SETCURRENT  40027
#define ID_STARTMEASUREMENT_SETVOLTAGE  40028
#define ID_CONFIGCURRENTSESSION_CYCLICVOLTAMMETRY 40029
#define ID_CONFIGCURRENTSESSION_REPEATINGCHRONOAMPEROMETRY 40030
#define ID_STARTMEASUREMENT_CYCLICVOLTAMMETRY 40031
#define ID_CONFIGCURRENTSESSION_CHRONOPOTENTIOMETRY 40032
#define ID_STARTMEASUREMENT_CHRONOPOTENTIOMETRY 40033
#define ID_CONFIGCURRENTSESSION_CONFIGCHRONO 40034
#define ID_STARTMEASUREMENT_CHRONO      40035
#define ID_HELP_STARTDEBUGCONSOL        40036

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        123
#define _APS_NEXT_COMMAND_VALUE         40037
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
