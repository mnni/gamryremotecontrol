//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: iniReader.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef INIREADER_H_INCLUDED__
#define INIREADER_H_INCLUDED__

#include <iostream>
#include <Windows.h>
#include <CkStringArray.h>
#include <CkString.h>

class CIniReader
{
public:
 CIniReader(); // default construtor 
 CIniReader(char* szFileName); 
 int ReadInteger(char* szSection, char* szKey, int iDefaultValue);
 float ReadFloat(char* szSection, char* szKey, float fltDefaultValue);
 bool ReadBoolean(char* szSection, char* szKey, bool bolDefaultValue);
 char* ReadString(char* szSection, char* szKey, const char* szDefaultValue);

 void getSectionNames(CkStringArray* sectionList);
 void getSectionData(CkStringArray* sectionList,CkString strSection);
 
 bool sectionExists(CkString strSection);

private:
  char m_szFileName[255];
  long m_lRetValue;
};
#endif//INIREADER_H_INCLUDED__