//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Gamry remote control interface
//
// Copyright (c) 2019 by Martin N�rby Nielsen
//
// This file is part of the Gamry remote control interface.
//
// Gamry remote control interface for gamry is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Gamry remote control interface for Gamry is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: gamrycontrol.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef GAMRYCONTROL_H_INCLUDED__
#define GAMRYCONTROL_H_INCLUDED__


#include "IniWriter.h"
#include "IniReader.h"
#include "iostream"
#include <stdio.h>
#include <string.h>

#include <Windows.h>
#include <sstream>

#include <direct.h>
#include <CkSocket.h>
#include <CkStringArray.h>
#include <CkString.h>

class gamryControl
{
	private:	

		



		CkString homeFolder;		
		CkString userName	;		// will be used to write folder path and location for active user.ini file
		CkString dirUserPath ;
		CkString control_INI_Path;
		CkString conf_ini_path; 
		
		CIniWriter testINIWrite; 
		CIniReader UserINIRead;			// point to:   /homefolder/user.ini
		CIniWriter UserINIWrite;		// point to:   /homefolder/user.ini
		CIniWriter iniWritercontrol ;	// point to:   /homefolder/users/userName/session_control.INI	
		CIniReader iniReadercontrol ;	// point to:   /homefolder/users/userName/session_control.INI
		CIniWriter iniWriterSetup ;		// point to:   /homefolder/setup.ini
		CIniReader iniReaderSetup ;		// point to:   /homefolder/setup.ini
		// should maybe allso have one for the current session /homefolder/users/$userName/$session/sessionconf.ini
		CIniWriter iniWriterSessionConf ;		// point to:   /homefolder/users/$userName/$session/sessionconf.ini
		CIniReader iniReaderSessionConf ;		// point to:   /homefolder/users/$userName/$session/sessionconf.ini


		void createNewControlIni(); 
	public:
	
		gamryControl(const char *homeFolderPath );
		void resetUserControlIni();
	
		void setEthsettings(char *IP,  unsigned int PORT);
		unsigned int getEthPort();
		CkString getEthIP(); 
		bool userExisted();
		bool setUserAndSession( const char* thisUserName, unsigned int thisSessionNum);
		bool setUser(const char* thisUserName); // set the user to the current session
		unsigned int incrSessionNum();
		bool setSessionNum(unsigned int sessionNum);
		unsigned int getTotalNumSession(const char* thisUserName);
		unsigned int getCurrentSessionNum(const char* thisUserName);
		CkString getCurrentUserName();
		
		bool createNewUser(const char* userName);
		void createHomeFolders();
		void createUserFolder();
		void createSessionFolders(unsigned int sessionNum);
		void createSessionConfFile(unsigned int sessionNum, unsigned int testnumber);
		void createSessionSetFile(unsigned int sessionNum);
		void creatDefaultUserIniFile();
		void creatDeafaultSetupFile();
		void creatDefaultSessionSetFile();
		CkString eis(const char* thisUserName , unsigned int sessionNum);
		CkString getFile(const char* thisUserName , unsigned int sessionNum , unsigned int fileNumber, const char* modeType);
		CkString getNewFile(const char* thisUserName , unsigned int sessionNum , unsigned int fileNumber, const char* modeType);
		CkString convertDtaToI2b(CkString dtaFileStream, const char* thisfileName);
		CkString exp_script(const char* thisUserName , unsigned int sessionNum, const char* scriptName);
		bool pstate();
		bool isValidUser(const char* thisUserName); // check if user is created 
		bool isValidSession(const char* thisUserName, unsigned int thisSessionNum); 
		CkString getSessionPath(const char* thisUserName, unsigned int thisSessionNum);
		
		bool configSessionConfFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key, char* Value);  //  this function should set the session sessionconf.ini file
		CkString readSessionConfFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key);
		 
		bool configSetFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key, char* Value);
		CkString readSetFile(const char* thisUserName,unsigned int thisSessionNum, char* sectionTag, char* key);

		void getAllSectionNames(CkStringArray* sectionNameList , const char* thisFile);
		void getAllSectionKeys(CkStringArray* keyList ,const char* thisFile , const char* sectionName);
		bool dosectionExists(const char* thisFile, CkString strSection);

		CkString loadFileRaw(const char* thisUserName,unsigned int thisSessionNum, char* FileType);
		bool saveFileRaw(const char* thisUserName,unsigned int thisSessionNum, char* FileType, CkString fileStream);
		CkString loadRawFile_EIS300(const char* thisUserName,unsigned int thisSessionNum);
		CkString loadRawFile_sessionConf(const char* thisUserName,unsigned int thisSessionNum);
		bool saveFileRaw_EIS300(const char* thisUserName,unsigned int thisSessionNum, CkString fileStream);
		bool saveFileRaw_sessionConf(const char* thisUserName,unsigned int thisSessionNum, CkString fileStream);
		CkString openFrameworkConfMenu(const char* thisUserName , unsigned int sessionNum, const char* menuItem);
		CkString chrono(const char* thisUserName , unsigned int sessionNum);
		CkString cyclicVoltammetry(const char* thisUserName , unsigned int sessionNum);
		
		
		CkString setCurrent(const char* thisUserName , unsigned int sessionNum,const char* setPoint);
		CkString setVoltage(const char* thisUserName , unsigned int sessionNum,const char* setPoint);
		CkString measureCurrent(const char* thisUserName , unsigned int sessionNum);
		CkString measureVoltage(const char* thisUserName , unsigned int sessionNum);

		bool writeLogFile(const char* thisUserName , unsigned int sessionNum, CkString newLine);
		CkString readLogFile(const char* thisUserName , unsigned int sessionNum); 
};
#endif//GAMRYCONTROL_H_INCLUDED__